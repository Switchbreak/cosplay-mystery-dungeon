MXMLC = /Users/lsd/flex_sdk/bin/mxmlc
#FLIXEL = /home/lsd/projects/flash-test/ChevyRay-FlashPunk-46c75be
SRC = src/FloorMap.as src/GameWorld.as src/Main.as src/Player.as src/WorldItem.as src/UI/Button.as src/UI/Inventory.as src/UI/ItemDetailPane.as src/UI/ItemWindow.as src/Sound/RandomSound.as src/Items/EnergyDrink.as src/LoseScreen.as src/WinScreen.as
MAIN = src/Main.as
SWF = CosplayMysteryDungeon.swf
 
$(SWF) : $(SRC) $(MAIN)
	$(MXMLC) -sp FlashPunk -sp src -static-link-runtime-shared-libraries=true -debug=true -o $(SWF) -- $(MAIN)


