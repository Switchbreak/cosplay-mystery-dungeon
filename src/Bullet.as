package  
{
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.motion.LinearMotion;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Bullet extends Entity 
	{
		[Embed(source = "../img/Effects/laser.png")]	protected static const LASER:Class;
		[Embed(source = "../img/Effects/magic.png")]	protected static const MAGIC:Class;
		
		protected var bulletMotion:LinearMotion;
		
		public function Bullet( startX:int, startY:int, targetX:int, targetY:int, magic:Boolean = false ) 
		{
			layer = -2000;
			
			var bulletImage:Image
			if ( magic )
			{
				bulletImage = new Image( MAGIC, new Rectangle( 0, 0, 64, 64 ) );
			}
			else
			{
				bulletImage = new Image( LASER );
			}
			bulletImage.centerOrigin();
			bulletImage.angle = FP.angle( startX, startY, targetX, targetY );
			
			super( startX, startY, bulletImage );
			
			bulletMotion = new LinearMotion( Finish, Tween.ONESHOT );
			bulletMotion.setMotion( startX, startY, targetX, targetY, 0.5 );
			addTween( bulletMotion, true );
		}
		
		override public function update():void 
		{
			x = bulletMotion.x;
			y = bulletMotion.y;
			
			super.update();
		}
		
		public function Finish():void
		{
			world.remove( this );
		}
	}
}