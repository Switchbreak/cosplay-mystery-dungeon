package  
{
	import flash.display.BitmapData;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Tilemap;
	import Types.BSPNode;
	import Types.MapPosition;
	
	/**
	 * Handles the map of the current floor of the dungeon
	 * @author Switchbreak
	 */
	public class FloorMap extends Entity 
	{
		// Embeds
		[Embed(source = "../img/Tiles/WhiteSheet.png")]		private static const WHITE_SHEET:Class;
		[Embed(source = "../img/Tiles/TabletopSheet.png")]	private static const TABLETOP_SHEET:Class;
		[Embed(source = "../img/Tiles/ArcadeSheet.png")]	private static const ARCADE_SHEET:Class;
		[Embed(source = "../img/Tiles/AnimeSheet.png")]		private static const ANIME_SHEET:Class;
		[Embed(source = "../img/Tiles/ConcreteSheet.png")]	private static const CONCRETE_SHEET:Class;
		[Embed(source = "../img/Tiles/HorrorSheet.png")]	private static const HORROR_SHEET:Class;
		[Embed(source = "../img/Tiles/HotelSheet.png")]		private static const HOTEL_SHEET:Class;
		
		// Constants
		static public const		MAP_WIDTH:uint				= 30;
		static public const		MAP_HEIGHT:uint				= 30;
		static public const		TILE_WIDTH:uint				= 64;
		static public const		TILE_HEIGHT:uint			= 64;
		static protected const	WALL_COUNT:int				= 75;
		static protected const	SIGHT_RANGE:int				= 4;
		static protected const	MIN_ROOM_SIZE:int			= 3;
		static protected const	ROOM_BORDER:int				= 2;
		static protected const	CORRIDOR_WIDTH:int			= 3;
		static protected const	MAP_COLORS:Vector.<uint>	= Vector.<uint>( [ 0xFF000000, 0xFFFF0000 ] );
		static protected const	MAP_RECURSION_LEVEL:int		= 3;
		static protected const	DECORATION_COUNT:int		= 4;
		static protected const	TILESETS:Array				= [WHITE_SHEET, CONCRETE_SHEET, TABLETOP_SHEET, ARCADE_SHEET, ANIME_SHEET, HORROR_SHEET, HOTEL_SHEET];
		static protected const	FLOOR_NAMES:Array			= ["Lobby", "Dealer Room", "Tabletop Room", "Arcade", "Anime Room", "Horror", "Hotel Suite"];
		static public const		TILESET_COUNT:int			= TILESETS.length << 1;
		
		// Tile types
		static public const TILE_HIDDEN:uint		= 0;
		static public const TILE_CORRIDOR:uint		= 1;
		static public const TILE_DECORATION:uint	= 2;
		static public const TILE_FLOOR:uint			= 3;
		static public const TILE_WALL:uint			= 4;
		static public const TILE_CORNER:uint		= 8;
		static public const TILE_OUTER_CORNER:uint	= 12;
		
		// Game tile types
		static public const	GAME_TILE_FLOOR:uint		= 0;
		static public const GAME_TILE_WALL:uint			= 1;
		static public const GAME_TILE_NOT_FOUND:uint	= 2;
		
		// Variables
		protected var	map:Vector.<int>;
		protected var	backgroundTiles:Tilemap;
		protected var	foregroundTiles:Tilemap;
		protected var	mapDisplay:Image;
		protected var	eventPopulateRoom:Function;
		protected var	rooms:Vector.<Rectangle>;
		public var		floorName:String;
		
		/**
		 * Create and initialize the floor map
		 * @param	setEventPopulateRoom	Function to call when a room is
		 * 									created to fill it with game items
		 * @param	currentFloor			Select which tileset to use
		 */
		public function FloorMap( setEventPopulateRoom:Function, currentFloor:int ) 
		{
			var tileSet:Class = TILESETS[(currentFloor >> 1)];
			floorName = FLOOR_NAMES[(currentFloor >> 1)];
			backgroundTiles = new Tilemap( tileSet, MAP_WIDTH * TILE_WIDTH, MAP_HEIGHT * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT );
			
			foregroundTiles = new Tilemap( tileSet, MAP_WIDTH * TILE_WIDTH, MAP_HEIGHT * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT );
			foregroundTiles.floodFill( 0, 0, TILE_HIDDEN );
			
			super( 0, 0, new Graphiclist( backgroundTiles, foregroundTiles ) );
			
			eventPopulateRoom = setEventPopulateRoom;
			
			rooms = new Vector.<Rectangle>();
			GenerateLevel();
		}
		
		/**
		 * Gets a bitmap representing a map of the level layout
		 * @return	Level map bitmap
		 */
		public function GetMapBitmap():BitmapData
		{
			var mapBitmap:BitmapData = new BitmapData( MAP_WIDTH, MAP_HEIGHT, false );
			for ( var row:int = 0; row < MAP_HEIGHT; ++row )
			{
				for ( var col:int = 0; col < MAP_WIDTH; ++col )
				{
					var color:uint = 0x808080;
					if ( !TileHidden( row, col ) )
					{
						color = MAP_COLORS[GetTileAtLocation( row, col )];
					}
					mapBitmap.setPixel( col, row, color );
				}
			}
			
			return mapBitmap;
		}
		
		/**
		 * Loads the generated level into the background tilemap
		 */
		protected function LoadLevelTilemap():void 
		{
			for ( var row:int = 0; row < MAP_HEIGHT; ++row )
			{
				for ( var col:int = 0; col < MAP_WIDTH; ++col )
				{
					backgroundTiles.setTile( col, row, map[row * MAP_WIDTH + col] );
				}
			}
			
			backgroundTiles.ApplyFilter( new BlurFilter( 2, 2, BitmapFilterQuality.HIGH ) );
		}
		
		/**
		 * Procedurally build the level layout
		 */
		protected function GenerateLevel():void
		{
			map = new Vector.<int>( MAP_WIDTH * MAP_HEIGHT );
			for (var i:int = 0; i < map.length; i++) 
			{
				map[i] = TILE_WALL;
			}
			
			var space:BSPNode = new BSPNode();
			space.leaf = new Rectangle( 0, 0, MAP_WIDTH, MAP_HEIGHT );
			
			PartitionRoom( space, MAP_RECURSION_LEVEL );
			CreateRooms( space );
			SideWalls();
			LoadLevelTilemap();
			
			eventPopulateRoom( rooms );
		}
		
		/**
		 * Recursively subdivide a BSP node into two smaller ones if possible
		 * @param	space		BSP node to subdivide
		 * @param	level		Current recursion level, used to end loop
		 * @param	vertical	True if dividing vertically, false if horizontal
		 */
		protected function PartitionRoom( space:BSPNode, level:int, vertical:Boolean = true ):void
		{
			// Check minimum space available to subdivide - the space must be
			// large enough to hold two of the smallest room size, plus two
			// border walls each, plus one extra corridor space between them
			var divisionSpace:int = (vertical ? space.leaf.width : space.leaf.height);
			divisionSpace -= ((MIN_ROOM_SIZE + ROOM_BORDER) << 1) + CORRIDOR_WIDTH;
			if ( divisionSpace < 0 )
				return;
			
			var division:int = FP.rand( divisionSpace ) + MIN_ROOM_SIZE + ROOM_BORDER;
			space.position = CutNode( space, division, vertical );
			
			space.leaf = null;
			space.vertical = vertical;
			
			if ( level > 0 )
			{
				--level;
				PartitionRoom( space.leftChild, level, !vertical );
				PartitionRoom( space.rightChild, level, !vertical );
			}
		}
		
		/**
		 * Divides a BSP node into two smaller ones at a specified point
		 * @param	space		BSP node to divide
		 * @param	division	Axis-aligned point to divide on
		 * @param	vertical	True if dividing vertically, false if horizontal
		 * @return	Position of the division on the map
		 */
		protected function CutNode( space:BSPNode, division:int, vertical:Boolean ):int
		{
			space.leftChild = new BSPNode();
			space.rightChild = new BSPNode();
			
			if ( vertical )
			{
				space.leftChild.leaf = new Rectangle( space.leaf.x, space.leaf.y, division, space.leaf.height );
				space.rightChild.leaf = new Rectangle( space.leaf.x + division + CORRIDOR_WIDTH, space.leaf.y, space.leaf.width - division - CORRIDOR_WIDTH, space.leaf.height );
				return space.leaf.x + division + (CORRIDOR_WIDTH >> 1);
			}
			else
			{
				space.leftChild.leaf = new Rectangle( space.leaf.x, space.leaf.y, space.leaf.width, division );
				space.rightChild.leaf = new Rectangle( space.leaf.x, space.leaf.y + division + CORRIDOR_WIDTH, space.leaf.width, space.leaf.height - division - CORRIDOR_WIDTH );
				return space.leaf.y + division + (CORRIDOR_WIDTH >> 1);
			}
		}
		
		/**
		 * Creates rooms from leaves on a BSP tree
		 * @param	space		BSP node to traverse
		 * @param	vertical	True if parent BSP is sliced vertically, false
		 * 						if horizontal
		 */
		protected function CreateRooms( space:BSPNode, vertical:Boolean = true ):int
		{
			if ( space.leaf != null )
			{
				CarveRoom( space.leaf );
				DecorateRoom( space.leaf );
				rooms.push( space.leaf );
				if ( vertical )
				{
					return space.leaf.top + FP.rand( space.leaf.height - 2 ) + 1;
				}
				else
				{
					return space.leaf.left + FP.rand( space.leaf.width - 2 ) + 1;
				}
			}
			else
			{
				var leftDoorPos:int = CreateRooms( space.leftChild, space.vertical );
				var rightDoorPos:int = CreateRooms( space.rightChild, space.vertical );
				
				CarveCorridor( space, leftDoorPos, rightDoorPos );
				return space.position;
			}
		}
		
		/**
		 * Creates an empty room defined by a rectangle, with a 1-tile thick
		 * border around it
		 * @param	room	Rectangle defining the room
		 */
		protected function CarveRoom( room:Rectangle ):void
		{
			for ( var row:int = room.top + 1; row < room.bottom - 1; ++row )
			{
				for ( var column:int = room.left + 1; column < room.right - 1; ++column )
				{
					map[row * MAP_WIDTH + column] = TILE_CORRIDOR + FP.rand( 3 );
				}
			}
		}
		
		/**
		 * Places decorations randomly in a room
		 * @param	room	Rectangle defining the room
		 */
		protected function DecorateRoom( room:Rectangle ):void
		{
			var count:int = FP.rand( DECORATION_COUNT );
			
			for (var i:int = 0; i < count; i++) 
			{
				var row:int		= room.top + FP.rand( room.height - 2 ) + 1;
				var column:int	= room.left + FP.rand( room.width - 2 ) + 1;
				map[row * MAP_WIDTH + column] = TILE_DECORATION;
			}
		}
		
		/**
		 * Creates a corridor between two adjacent children of a BSP node
		 * @param	space		BSP node containing the two children
		 * @param	leftDoor	Position of the door on the left child node
		 * @param	rightDoor	Position of the door on the right child node
		 */
		protected function CarveCorridor( space:BSPNode, leftDoor:int, rightDoor:int ):void
		{
			if ( space.leaf != null )
				return;
			
			if ( space.vertical )
			{
				var column:int = space.position;
				CarveDirection( leftDoor, column, 0, -1 );
				CarveDirection( rightDoor, column, 0, 1 );
				
				if ( leftDoor > rightDoor )
				{
					var temp:int = rightDoor;
					rightDoor = leftDoor;
					leftDoor = temp;
				}
				
				for ( var row:int = leftDoor; row <= rightDoor; ++row )
				{
					map[row * MAP_WIDTH + column] = TILE_CORRIDOR + FP.rand( 3 );
				}
			}
			else 
			{
				row	= space.position;
				CarveDirection( row, leftDoor, -1, 0 );
				CarveDirection( row, rightDoor, 1, 0 );
				
				if ( leftDoor > rightDoor )
				{
					temp = rightDoor;
					rightDoor = leftDoor;
					leftDoor = temp;
				}
				
				for ( column = leftDoor; column <= rightDoor; ++column )
				{
					map[row * MAP_WIDTH + column] = TILE_CORRIDOR + FP.rand( 3 );
				}
			}
		}
		
		/**
		 * Carve in a specified direction until a floor space is hit
		 * @param	row		Starting position
		 * @param	column	Starting position
		 * @param	rowStep	Direction (should be 1 or -1, 0 if colStep is set)
		 * @param	colStep	Direction (should be 1 or -1, 0 if rowStep is set)
		 */
		protected function CarveDirection( row:int, column:int, rowStep:int, colStep:int ):void 
		{
			var carveRow:int = row;
			var carveCol:int = column;
			while ( true )
			{
				carveRow += rowStep;
				carveCol += colStep;
				
				if ( carveRow < 0 || carveRow >= MAP_HEIGHT || carveCol < 0 || carveCol >= MAP_WIDTH )
					return;
				
				if ( GetTileAtLocation( carveRow, carveCol ) == GAME_TILE_FLOOR )
					return;
				
				map[carveRow * MAP_WIDTH + carveCol] = TILE_CORRIDOR + FP.rand( 3 );
			}
		}
		
		/**
		 * Apply wall siding to all wall tiles
		 */
		protected function SideWalls():void
		{
			for ( var row:int = 0; row < MAP_HEIGHT; ++row )
			{
				for ( var col:int = 0; col < MAP_WIDTH; ++col )
				{
					if ( GetTileAtLocation( row, col ) == GAME_TILE_WALL )
						CellSiding( row, col );
				}
			}
		}
		
		/**
		 * Set walls to face adjacent floor cells
		 * THIS CODE IS HORRIBLE I HATE IT
		 * @param	row
		 * @param	column
		 */
		protected function CellSiding( row:int, column:int ):void
		{
			// Check four adjacent directions
			if ( GetTileAtLocation( row + 1, column ) == GAME_TILE_FLOOR )
			{
				if ( GetTileAtLocation( row, column - 1 ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER;
				else if ( GetTileAtLocation( row, column + 1 ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 3;
				else 
					map[row * MAP_WIDTH + column] = TILE_WALL;
				return;
			}
			if ( GetTileAtLocation( row, column - 1 ) == GAME_TILE_FLOOR )
			{
				if ( GetTileAtLocation( row - 1, column ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 1;
				else if ( GetTileAtLocation( row + 1, column ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 2;
				else 
					map[row * MAP_WIDTH + column] = TILE_WALL + 1;
				return;
			}
			if ( GetTileAtLocation( row - 1, column ) == GAME_TILE_FLOOR )
			{
				if ( GetTileAtLocation( row, column - 1 ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 1;
				else if ( GetTileAtLocation( row, column + 1 ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 2;
				else 
					map[row * MAP_WIDTH + column] = TILE_WALL + 2;
				return;
			}
			if ( GetTileAtLocation( row, column + 1 ) == GAME_TILE_FLOOR )
			{
				if ( GetTileAtLocation( row - 1, column ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER;
				else if ( GetTileAtLocation( row + 1, column ) == GAME_TILE_FLOOR )
					map[row * MAP_WIDTH + column] = TILE_OUTER_CORNER + 3;
				else 
					map[row * MAP_WIDTH + column] = TILE_WALL + 3;
				return;
			}
			
			// All adjacent tiles are walls, check for corners
			if ( GetTileAtLocation( row + 1, column + 1 ) == GAME_TILE_FLOOR )
			{
				map[row * MAP_WIDTH + column] = TILE_CORNER;
				return;
			}
			if ( GetTileAtLocation( row + 1, column - 1 ) == GAME_TILE_FLOOR )
			{
				map[row * MAP_WIDTH + column] = TILE_CORNER + 1;
				return;
			}
			if ( GetTileAtLocation( row - 1, column - 1 ) == GAME_TILE_FLOOR )
			{
				map[row * MAP_WIDTH + column] = TILE_CORNER + 2;
				return;
			}
			if ( GetTileAtLocation( row - 1, column + 1 ) == GAME_TILE_FLOOR )
			{
				map[row * MAP_WIDTH + column] = TILE_CORNER + 3;
				return;
			}
		}
		
		/**
		 * Gets the type of tile at a given map location
		 * @param	row		Map location
		 * @param	column	Map location
		 * @return	A tile index matching one of the Tile Type constants
		 */
		public function GetTileAtLocation( row:int, column:int ):uint
		{
			if ( row < 0 || row >= MAP_HEIGHT || column < 0 || column >= MAP_WIDTH )
				return GAME_TILE_NOT_FOUND;
				
			var tile:int = map[row * MAP_WIDTH + column];
			
			if ( tile >= TILE_CORRIDOR && tile < TILE_WALL )
				return GAME_TILE_FLOOR;
			else if( tile >= TILE_WALL && tile < TILE_OUTER_CORNER + 4 )
				return GAME_TILE_WALL;
			
			return GAME_TILE_NOT_FOUND;
		}
		
		/**
		 * Checks if a tile is obscured by fog of war
		 * @param	row		Position to check
		 * @param	column	Position to check
		 * @return	True if hidden, false if not
		 */
		public function TileHidden( row:int, column:int ):Boolean
		{
			return foregroundTiles.getTile( column, row ) == TILE_HIDDEN;
		}
		
		/**
		 * Display line of sight visually from a selected point on the map
		 * @param	row		Map location
		 * @param	column	Map location
		 */
		public function UpdateTileSight( row:int, column:int ):void 
		{
			for ( var backRow:int = row - SIGHT_RANGE - 1; backRow <= row + SIGHT_RANGE + 1; ++backRow )
			{
				for ( var backCol:int = column - SIGHT_RANGE - 1; backCol <= column + SIGHT_RANGE + 1; ++backCol )
				{
					if ( TileVisible( backRow, backCol, row, column ) )
					{
						foregroundTiles.setTile( backCol, backRow, backgroundTiles.getTile( backCol, backRow ) );
					}
					else if( !TileHidden( backRow, backCol ) )
					{
						foregroundTiles.clearTile( backCol, backRow );
					}
				}
			}
		}
		
		/**
		 * Check if a specified tile is visible from a given origin
		 * @param	row			Tile to check
		 * @param	column		Tile to check
		 * @param	originRow	Location to determine visibility from
		 * @param	originCol	Location to determine visibility from
		 * @return	True if visible, false if hidden
		 */
		protected function TileVisible( row:int, column:int, originRow:int, originCol:int ):Boolean 
		{
			if ( row < 0 || row >= MAP_HEIGHT )
				return false;
			if ( column < 0 || column >= MAP_WIDTH )
				return false;
			
			if ( (row - originRow) * (row - originRow) + (column - originCol) * (column - originCol) > (SIGHT_RANGE * SIGHT_RANGE) )
				return false;
			
			return LineOfSight( row, column, originRow, originCol );
		}
		
		/**
		 * Calculates if a tile has a clear line of sight from a given origin,
		 * using Bresenham's algorithm
		 * @param	row			Tile to check
		 * @param	column		Tile to check
		 * @param	originRow	Origin tile
		 * @param	originCol	Origin tile
		 * @return	True if line of sight is clear, false if obstructed
		 */
		public function LineOfSight( row:int, column:int, originRow:int, originCol:int ):Boolean
		{
			var steep:Boolean = Math.abs( row - originRow ) > Math.abs( column - originCol );
			if ( steep )
			{
				var temp:int;
				
				temp = row;
				row = column;
				column = temp;
				
				temp = originRow;
				originRow = originCol;
				originCol = temp;
			}
			
			var deltaCol:int = Math.abs( column - originCol );
			var deltaRow:int = Math.abs( row - originRow );
			var error:int = deltaCol >> 1;
			var xstep:int = (originCol < column ? 1 : -1);
			var ystep:int = (originRow < row ? 1 : -1);
			var y:int = originRow;
			
			for ( var x:int = originCol; x != column; x += xstep )
			{
				var tile:int;
				if ( steep )
					tile = GetTileAtLocation( x, y );
				else 
					tile = GetTileAtLocation( y, x );
				
				if ( tile != GAME_TILE_FLOOR )
					return false;
				
				error -= deltaRow;
				if ( error < 0 )
				{
					y += ystep;
					error += deltaCol;
				}
			}
			
			return true;
		}
		
		/**
		 * Make whole map visible (CHEAT!)
		 */
		public function ClearMap():void
		{
			foregroundTiles.clearRect( 0, 0, MAP_WIDTH, MAP_HEIGHT );
		}
	}
}