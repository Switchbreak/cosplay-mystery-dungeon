package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	
	/**
	 * A game object is a type of entity that lives on the map
	 * @author Switchbreak
	 */
	public class GameObject extends Entity 
	{
		// Variables
		public var	row:int;
		public var	column:int;
		public var	blocking:Boolean;
		
		/**
		 * Constructor. Can be usd to place the Entity and assign a graphic and mask.
		 * @param	setRow		Position to place the Entity.
		 * @param	setColumn	Position to place the Entity.
		 * @param	setBlocking	Set if the entity can be walked through or not
		 * @param	graphic		Graphic to assign to the Entity.
		 * @param	mask		Mask to assign to the Entity.
		 */
		public function GameObject( setRow:int, setColumn:int, setBlocking:Boolean, graphic:Graphic=null, mask:Mask=null )
		{
			row			= setRow;
			column		= setColumn;
			blocking	= setBlocking;
			
			super(column * FloorMap.TILE_WIDTH + (FloorMap.TILE_WIDTH >> 1), row * FloorMap.TILE_HEIGHT + (FloorMap.TILE_HEIGHT >> 1), graphic, mask);
			
			active	= false;
			layer	= -1 - row;
		}
		
		/**
		 * Called when the player walks into this object - override to add
		 * behavior
		 */
		public function Activate( target:Object ):void { }
	}

}