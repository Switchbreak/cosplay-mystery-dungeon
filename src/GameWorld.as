package  
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import Items.Comic;
	import Items.Money;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	import net.flashpunk.Sfx;
	import Objects.Brother;
	import Objects.Stairs;
	import Objects.Vendor;
	import Types.Item;
	import Types.MapPosition;
	import UI.Fader;
	import UI.HealthBar;
	import UI.Inventory;
	import UI.QuestLog;
	
	/**
	 * Game world
	 * @author Switchbreak
	 */
	public class GameWorld extends World 
	{
		// Assets
		[Embed(source = "../font/MP16REG.ttf", embedAsCFF = 'false', fontFamily = 'UI Font')] protected static const UI_FONT:Class;
		[Embed(source = "../snd/music.mp3")] protected static const BACKGROUND_MUSIC_SND:Class;
		
		// Constants
		public static const		HUD_LAYER:int			= -30000;
		static protected const	MAP_DISPLAY_WIDTH:int	= 400;
		static protected const	MAP_DISPLAY_HEIGHT:int	= 400;
		static protected const	PLAYER_MAP_COLOR:uint	= 0xFFFF00;
		static protected const	ITEM_MAP_COLOR:uint		= 0x0000FF;
		static protected const	MONSTER_MAP_COLOR:uint	= 0x00FF00;
		static protected const	OBJECT_MAP_COLOR:uint	= 0x00FFFF;
		static protected const	CAMERA_LERP:Number		= 5;
		static protected const	ITEM_DENSITY:Number		= 0.01;
		static protected const	MONSTER_DENSITY:Number	= 0.07;
		static protected const	MONSTER_COUNT:int		= 15;
		static protected const	WELCOME_TEXT:String		= "Welcome to Cosplay Mystery Dungeon";
		static protected const	DESCEND_TEXT:String		= "You descend a dark staircase to a new floor.";
		static protected const	FLOOR_NAME_X:int		= 10;
		static protected const	FLOOR_NAME_Y:int		= 3;
		static protected const	MONEY_COUNTER_X:int		= 10;
		static protected const	MONEY_COUNTER_Y:int		= 50;
		
		// Key bind names
		static protected const	KEY_RESET:String		= "Reset";
		static protected const	KEY_MAP:String			= "Map";
		static protected const	KEY_INVENTORY:String	= "Inventory";
		
		// Variables
		public static var	currentFloor:FloorMap;
		public static var	player:Player;
		public static var	inventory:Inventory;
		public static var	log:QuestLog;
		public static var	items:Vector.<Item>;
		public static var	floorNumber:int;
		protected var		map:Entity;
		protected var		healthBar:HealthBar;
		protected var		moneyCounter:Text;
		protected var		floorName:Text;
		protected var		waitingOn:int;
		protected var		fader:Fader;
		
		// Sounds
		protected var backgroundMusic:Sfx = new Sfx(BACKGROUND_MUSIC_SND);
		/**
		 * Create and initialize the game world
		 * @param	setPlayer	Set to persist a player from another world
		 */
		public function GameWorld( setPlayer:Player = null, setQuestLog:QuestLog = null, setFloor:int = 0 ) 
		{
			FP.randomizeSeed();
			
			Text.font	= "UI Font";
			items		= new Vector.<Item>();
			floorNumber	= setFloor;
			
			Input.define( KEY_RESET, Key.R );
			Input.define( KEY_MAP, Key.M );
			Input.define( KEY_INVENTORY, Key.I );
			
			fader = new Fader();
			add( fader );
			
			if ( floorNumber == 0 )
			{
				Comic.identified = null;
			}
			
			currentFloor = new FloorMap( PopulateRooms, floorNumber );
			add( currentFloor );
			
			for (var i:int = 0; i < MONSTER_COUNT; i++) 
			{
				while ( true )
				{
					var row:int		= FP.rand( FloorMap.MAP_HEIGHT );
					var column:int	= FP.rand( FloorMap.MAP_WIDTH );
					if ( currentFloor.GetTileAtLocation( row, column ) == FloorMap.GAME_TILE_FLOOR )
					{
						add( ObjectFactory.CreateRandomMonster(	row, column ) );
						break;
					}
				}
			}
			
			if ( setPlayer == null )
				player = new Player();
			else
				player = setPlayer;
			add( player );
			
			if ( player.inventory.length > 0 )
			{
				for each( var item:Item in player.inventory )
					items.push( item );
			}
			
			if ( setQuestLog == null )
			{
				log = new QuestLog();
				log.AddLine( WELCOME_TEXT );
			}
			else
			{
				log = setQuestLog;
			}
			add( log );
			
			floorName = new Text( currentFloor.floorName, FLOOR_NAME_X, FLOOR_NAME_Y, { scrollX: 0, scrollY: 0 } );
			addGraphic( floorName, HUD_LAYER );
			
			healthBar = new HealthBar();
			add( healthBar );
			
			moneyCounter = new Text( "$" + player.money.toFixed( 2 ), MONEY_COUNTER_X, MONEY_COUNTER_Y, { scrollX: 0, scrollY: 0 } );
			addGraphic( moneyCounter, HUD_LAYER );
			
			map = new Entity();
			map.layer	= HUD_LAYER;
			map.active	= false;
			map.visible	= false;
			add( map );
			
			inventory = new Inventory();
			add( inventory );
		}
		
		override public function begin():void 
		{
			GameObjectFogOfWar();
			backgroundMusic.loop(0.8);
			super.begin();
		}
		
		override public function end():void
		{
		    backgroundMusic.stop();
		    super.end();
		}
		
		/**
		 * Main game loop
		 */
		override public function update():void 
		{
			if ( !player.dead )
			{
				if ( Input.pressed( KEY_MAP ) )
				{
					ShowMap();
				}
				if ( Input.released( KEY_MAP ) )
				{
					HideMap();
				}
				if ( Input.pressed( KEY_INVENTORY ) )
				{
					ToggleInventory();
				}
				
				// CHEATS! Remove these!
				//if ( Input.pressed( Key.DIGIT_1 ) )
				//{
					//currentFloor.ClearMap();
				//}
				//if ( Input.pressed( Key.DIGIT_2 ) )
				//{
					//player.Heal( 100 );
				//}
				//if ( Input.pressed( Key.DIGIT_3 ) )
				//{
					//var money:Money = new Money();
					//items.push( money );
					//player.PickUp( money );
				//}
				//if ( Input.pressed( Key.DIGIT_4 ) )
				//{
					//var comic:Comic = new Comic();
					//items.push( comic );
					//player.PickUp( comic );
				//}
				//if ( Input.pressed( Key.DIGIT_5 ) )
				//{
					//var item:Item = ObjectFactory.CreateRandomItem();
					//items.push( item );
					//player.PickUp( item );
				//}
				//if ( Input.pressed( KEY_RESET ) )
				//{
					//Descend();
				//}
			}
			
			super.update();
			
			CameraFollow( player.x, player.y );
		}
		
		/**
		 * Advance the simulation by one turn
		 */
		public function AdvanceTurn():void
		{
			WaitOnAction();
			
			if ( player.confused )
			{
				if ( --player.confusedTimer <= 0 )
					player.confused = false;
			}
			
			// Update all items each turn
			for each( var item:Item in items )
			{
				if ( item.active )
					item.Update();
			}
			
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			getClass( Monster, monsters );
			
			for each( var monster:Monster in monsters )
			{
				if( !monster.dead )
					monster.TakeTurn();
			}
			
			GameObjectFogOfWar();
			
			EndTurn();
		}
		
		/**
		 * Do not end the current turn until some action that has started is
		 * complete. Responsibility is on the caller of this function to call
		 * EndTurn() to unblock!
		 */
		public function WaitOnAction():void
		{
			++waitingOn;
		}
		
		/**
		 * Called by any objects in the turn that were blocking the end of the
		 * turn until some action completes, advances to the player's turn when
		 * all actions have completed
		 */
		public function EndTurn():void
		{
			if ( --waitingOn <= 0 )
			{
				waitingOn = 0;
				player.playersTurn = true;
			
				// Update UI elements
				if ( map.visible )
					UpdateMap();
				inventory.UpdateInventory();
				healthBar.UpdateBar();
				UpdateMoneyCounter();
			}
		}
		
		/**
		 * Update the money counter UI
		 */
		public function UpdateMoneyCounter():void
		{
			moneyCounter.text = "$" + player.money.toFixed( 2 );
		}
		
		/**
		 * Descend to next level
		 */
		public function Descend():void
		{
			if ( floorNumber + 1 < FloorMap.TILESET_COUNT )
			{
				log.AddLine( DESCEND_TEXT );
				fader.Start( true, 0.5, NextFloor );
			}
		}
		
		public function NextFloor():void
		{
			ResetWorld( player, log, floorNumber + 1 );
		}
		
		public function FadeOut( lose:Boolean = true ):void
		{
			fader.Start( true, 0.5, (lose ? LoseGame : WinGame) );
		}
		
		public function LoseGame():void
		{
			removeAll();
			FP.world = new LoseScreen();
		}
		
		public function WinGame():void
		{
			removeAll();
			FP.world = new WinScreen();
		}
		
		/**
		 * Reset the world and regenerate it
		 * @param	keepPlayer	Pass in current player object to persist it
		 */
		protected function ResetWorld( keepPlayer:Player = null, keepLog:QuestLog = null, setFloor:int = 0 ):void
		{
			removeAll();
			FP.world = new GameWorld( keepPlayer, keepLog, setFloor );
		}
		
		/**
		 * Apply fog of war to game objects
		 */
		protected function GameObjectFogOfWar():void 
		{
			var worldItems:Vector.<WorldItem> = new Vector.<WorldItem>();
			getClass( WorldItem, worldItems );
			for each( var worldItem:WorldItem in worldItems )
			{
				worldItem.visible = !currentFloor.TileHidden( worldItem.row, worldItem.column );
			}
			
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			getClass( Monster, monsters );
			for each( var monster:Monster in monsters )
			{
				monster.visible = !currentFloor.TileHidden( monster.row, monster.column );
			}
			
			var objects:Vector.<GameObject> = new Vector.<GameObject>();
			getClass( GameObject, objects );
			for each( var object:GameObject in objects )
			{
				object.visible = !currentFloor.TileHidden( object.row, object.column );
			}
		}
		
		/**
		 * Point the camera at a position
		 * @param	x	Target position
		 * @param	y	Target position
		 */
		protected function CameraFollow( x:int, y:int ):void
		{
			camera.x += (x - FP.halfWidth - camera.x) / CAMERA_LERP;
			camera.y += (y - FP.halfHeight - camera.y) / CAMERA_LERP;
		}
		
		/**
		 * Toggle whether the inventory bar UI element is displayed
		 */
		protected function ToggleInventory():void
		{
			inventory.enabled = !inventory.enabled;
		}
		
		/**
		 * Populate all rooms on the map with items and monsters
		 * @param	rooms	List of rooms
		 */
		protected function PopulateRooms( rooms:Vector.<Rectangle> ):void 
		{
			for each( var room:Rectangle in rooms )
			{
				PopulateRoom( room );
			}
			
			PlaceVendor( rooms[FP.rand( rooms.length )] );
			
			if( floorNumber < FloorMap.TILESET_COUNT - 1 )
				PlaceStairs( rooms[FP.rand( rooms.length )] );
			else
				PlaceBrother( rooms[FP.rand( rooms.length )] );
		}
		
		/**
		 * Sprinkle items randomly around a specified room
		 * @param	room	Rectangle defining the room
		 */
		protected function PopulateRoom( room:Rectangle ):void
		{
			var density:int = Math.max( (room.width - 2) * (room.height - 2) * ITEM_DENSITY, 2 );
			var count:int = FP.rand( density );
			
			for (var i:int = 0; i < count; i++) 
			{
				SpawnItem(	ObjectFactory.CreateRandomItem(),
							room.top + FP.rand( room.height - 2 ) + 1,
							room.left + FP.rand( room.width - 2 ) + 1 );
			}
			
			//density = Math.max( (room.width - 2) * (room.height - 2) * MONSTER_DENSITY, 3 );
			//count = FP.rand( density );
			//
			//for (i = 0; i < count; i++)
			//{
				//add( ObjectFactory.CreateRandomMonster(	room.top + FP.rand( room.height - 2 ) + 1,
														//room.left + FP.rand( room.width - 2 ) + 1 ) );
			//}
		}
		
		/**
		 * Spawns an item at a location
		 * @param	row
		 * @param	column
		 */
		public function SpawnItem( item:Item, row:int, column:int ):void
		{
			add( new WorldItem( row, column, item ) );
			items.push( item );
		}
		
		/**
		 * Place stairs in a selected room
		 * @param	room	Rectangle defining the room
		 */
		protected function PlaceStairs( room:Rectangle ):void
		{
			add( new Stairs( room.top + FP.rand( room.height - 2 ) + 1, room.left + FP.rand( room.width - 2 ) + 1 ) );
		}
		
		/**
		 * Place stairs in a selected room
		 * @param	room	Rectangle defining the room
		 */
		protected function PlaceBrother( room:Rectangle ):void
		{
			add( new Brother( room.top + FP.rand( room.height - 2 ) + 1, room.left + FP.rand( room.width - 2 ) + 1 ) );
		}
		
		/**
		 * Place stairs in a selected room
		 * @param	room	Rectangle defining the room
		 */
		protected function PlaceVendor( room:Rectangle ):void
		{
			add( new Vendor( room.top + FP.rand( room.height - 2 ) + 1, room.left + FP.rand( room.width - 2 ) + 1 ) );
		}
		
		/**
		 * Generate and show the current map
		 */
		protected function ShowMap():void
		{
			UpdateMap();
			map.visible = true;
		}
		
		/**
		 * Update the map's bitmap
		 */
		protected function UpdateMap():void
		{
			var mapBitmap:BitmapData = currentFloor.GetMapBitmap();
			MarkMap( mapBitmap );
			
			var mapDisplay:Image = new Image( mapBitmap );
			mapDisplay.scrollX = mapDisplay.scrollY = 0;
			mapDisplay.centerOrigin();
			mapDisplay.x = FP.halfWidth - (mapDisplay.width >> 1);
			mapDisplay.y = FP.halfHeight - (mapDisplay.height >> 1);
			mapDisplay.scaleX = MAP_DISPLAY_WIDTH / mapDisplay.width;
			mapDisplay.scaleY = MAP_DISPLAY_HEIGHT / mapDisplay.height;
			
			map.graphic = mapDisplay;
		}
		
		/**
		 * Mark player and game object positions on the map
		 * @param	mapBitmap	Map bitmap to mark
		 */
		protected function MarkMap( mapBitmap:BitmapData ):void
		{
			var worldItems:Vector.<WorldItem> = new Vector.<WorldItem>();
			getClass( WorldItem, worldItems );
			for each( var worldItem:WorldItem in worldItems )
			{
				if ( worldItem.visible )
					mapBitmap.setPixel( worldItem.column, worldItem.row, ITEM_MAP_COLOR );
			}
			
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			getClass( Monster, monsters );
			for each( var monster:Monster in monsters )
			{
				if ( monster.visible )
					mapBitmap.setPixel( monster.column, monster.row, MONSTER_MAP_COLOR );
			}
			
			
			var objects:Vector.<GameObject> = new Vector.<GameObject>();
			getClass( GameObject, objects );
			for each( var object:GameObject in objects )
			{
				if( object.visible )
					mapBitmap.setPixel( object.column, object.row, OBJECT_MAP_COLOR );
			}
			
			mapBitmap.setPixel( player.column, player.row, PLAYER_MAP_COLOR );
		}
		
		/**
		 * Hide the map
		 */
		protected function HideMap():void
		{
			map.visible = false;
		}
	}
}