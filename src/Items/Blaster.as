package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	import Types.Item;
	
	/**
	 * Blaster game item
	 * @author Switchbreak
	 */
	public class Blaster extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/blaster.png")]		private static const BLASTER:Class;
		[Embed(source = "../../snd/Weapons/blaster.mp3")] 	private static const BLASTER_SND:Class;
		
		static protected var blasterSound:RandomSound = new RandomSound(
		    [BLASTER_SND],
		    0.15);
		
		/**
		 * Create and initialize the item
		 */
		public function Blaster() 
		{
			name			= "Blaster";
			description		= "They don't allow realistic-looking guns at this con.";
			graphic			= new Stamp( BLASTER );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_SINGLE_HANDED;
			wearable		= false;
			active			= false;
			width			= 64;
			height			= 64;
			attack			= 10;
			value			= 15;
		}
		
		/**
		 * Play blaster sound when fired
		 */
		override public function Use(subject:Player, object:Monster):void 
		{
			blasterSound.play();
			
			super.Use(subject, object);
		}
	}
}