package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	import Types.Item;
	
	/**
	 * Blaster game item
	 * @author Switchbreak
	 */
	public class Blaster2 extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/blaster2.png")]		private static const BLASTER:Class;
		[Embed(source = "../../snd/Weapons/blaster.mp3")] 	private static const BLASTER_SND:Class;
		
		static protected var blasterSound:RandomSound = new RandomSound(
		    [BLASTER_SND],
		    0.15);
		
		/**
		 * Create and initialize the item
		 */
		public function Blaster2() 
		{
			name			= "Blaster Level 2";
			description		= "They don't allow realistic-looking guns at this con.";
			graphic			= new Stamp( BLASTER );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_SINGLE_HANDED;
			wearable		= false;
			active			= false;
			width			= 64;
			height			= 64;
			attack			= 30;
			value			= 30;
		}
		
		/**
		 * Play blaster sound when fired
		 */
		override public function Use(subject:Player, object:Monster):void 
		{
			blasterSound.play();
			
			super.Use(subject, object);
		}
	}
}