package Items 
{
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	import Objects.Flame;
	import Objects.GasCloud;
	import Objects.IceBolt;
	import Objects.LandMine;
	import Objects.LightningBolt;
	import Types.Item;
	import UI.DeathFloater;
	
	/**
	 * Comic book item
	 * @author Switchbreak
	 */
	public class Comic extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/comics.png")]    		protected static const COMIC_BOOK:Class;
		[Embed(source = "../../snd/Spells/confusion.mp3")]		protected static const CONFUSION_SFX:Class;
		[Embed(source = "../../snd/Spells/fireblast.mp3")]		protected static const FIRE_BLAST_SFX:Class;
		[Embed(source = "../../snd/Spells/freeze.mp3")]			protected static const FREEZE_SFX:Class;
		[Embed(source = "../../snd/Spells/lightningbolt.mp3")]	protected static const LIGHTNING_BOLT_SFX:Class;
		[Embed(source = "../../snd/Spells/megaheal.mp3")]		protected static const MEGA_HEAL_SFX:Class;
		[Embed(source = "../../snd/Spells/poisongas.mp3")]		protected static const POISON_GAS_SFX:Class;
		[Embed(source = "../../snd/Spells/teleport.mp3")]		protected static const TELEPORT_SFX:Class;
		
		// Constants
		protected static const NAME:String			= "Comic Book";
		protected static const DESCRIPTION:String	= "A comic book containing strange runes which seem to be enchanted. ";
		protected static const SPRITE_WIDTH:int		= 64;
		protected static const SPRITE_HEIGHT:int	= 64;
		protected static const HEAL_AMOUNT:int		= 20;
		protected static const HEAL_RANGE:int		= 5;
		protected static const EFFECT_COUNT:int		= 10;
		protected static const MEGA_HEAL_AMOUNT:int	= 100;
		
		// Effect descriptions
		protected static const FIRE_BLAST:int		= 0;
		protected static const MEGA_HEAL:int		= 1;
		protected static const LIGHTNING:int		= 2;
		protected static const LAND_MINE:int		= 3;
		protected static const TELEPORT:int			= 4;
		protected static const REVEAL:int			= 5;
		protected static const GENOCIDE:int			= 6;
		protected static const FREEZE:int			= 7;
		protected static const CONFUSION:int		= 8;
		protected static const POISON_GAS:int		= 9;
		
		protected static const FIRE_BLAST_DESC:String	= "Fires a blast of flame in all circles around you.";
		protected static const MEGA_HEAL_DESC:String	= "Recovers all of your HP.";
		protected static const LIGHTNING_DESC:String	= "Fires a long lightning bolt in front of you.";
		protected static const LAND_MINE_DESC:String	= "Creates an exploding trap.";
		protected static const TELEPORT_DESC:String		= "Teleports you, but where to?";
		protected static const REVEAL_DESC:String		= "Gives you a good view of your surroundings.";
		protected static const GENOCIDE_DESC:String		= "Kills all of one randomly chosen enemy type.";
		protected static const FREEZE_DESC:String		= "Freezes an enemy and holds them still.";
		protected static const CONFUSION_DESC:String	= "Has a dizzying effect.";
		protected static const POISON_GAS_DESC:String	= "Spews a cloud of toxic gas.";
		
		protected static const FIRE_BLAST_LOG:String	= "A blast of flame shoots out of the book.";
		protected static const MEGA_HEAL_LOG:String		= "You feel more healthy.";
		protected static const LIGHTNING_LOG:String		= "A powerful lightning bolt fires from the book.";
		protected static const LAND_MINE_LOG:String		= "An exploding trap appears.";
		protected static const TELEPORT_LOG:String		= "You find yourself in unfamiliar territory.";
		protected static const REVEAL_LOG:String		= "You feel you suddenly know your way around.";
		protected static const GENOCIDE_LOG:String		= "Something bad happened. To someone else.";
		protected static const FREEZE_LOG:String		= "A bolt of ice fires from the book.";
		protected static const CONFUSION_LOG:String		= "You don't feel so good.";
		protected static const POISON_GAS_LOG:String	= "A cloud of fumes pours from the book.";
		
		protected static const EFFECT_DESCS:Array = [FIRE_BLAST_DESC, MEGA_HEAL_DESC, LIGHTNING_DESC, LAND_MINE_DESC, TELEPORT_DESC, REVEAL_DESC, GENOCIDE_DESC, FREEZE_DESC, CONFUSION_DESC, POISON_GAS_DESC];
		protected static const EFFECT_LOGS:Array = [FIRE_BLAST_LOG, MEGA_HEAL_LOG, LIGHTNING_LOG, LAND_MINE_LOG, TELEPORT_LOG, REVEAL_LOG, GENOCIDE_LOG, FREEZE_LOG, CONFUSION_LOG, POISON_GAS_LOG];
		
		// Variables
		public static var	identified:Vector.<Boolean> = null;
		public var			comicType:int;
		
		protected static var confusionSfx:Sfx	= new Sfx( CONFUSION_SFX );
		protected static var fireblastSfx:Sfx	= new Sfx( FIRE_BLAST_SFX );
		protected static var freezeSfx:Sfx		= new Sfx( FREEZE_SFX );
		protected static var lightningSfx:Sfx	= new Sfx( LIGHTNING_BOLT_SFX );
		protected static var megahealSfx:Sfx	= new Sfx( MEGA_HEAL_SFX );
		protected static var poisonGasSfx:Sfx	= new Sfx( POISON_GAS_SFX );
		protected static var teleportSfx:Sfx	= new Sfx( TELEPORT_SFX );
		
		/**
		 * Create and initialize the comic book
		 */
		public function Comic() 
		{
			if ( identified == null )
				identified = new Vector.<Boolean>( EFFECT_COUNT );
			
			name		= NAME;
			description	= DESCRIPTION;
			usable		= true;
			throwable	= true;
			equipable	= EQUIPABLE_NOT;
			wearable	= false;
			active		= false;
			width		= SPRITE_WIDTH;
			height		= SPRITE_HEIGHT;
			value		= 10;
			
			comicType = FP.rand( EFFECT_COUNT );
			if ( identified[comicType] )
				description += EFFECT_DESCS[comicType];
			
			var comic:Spritemap	= new Spritemap( COMIC_BOOK, SPRITE_WIDTH, SPRITE_HEIGHT );
			comic.active		= false;
			comic.frame			= comicType;
			graphic				= comic;
		}
		
		/**
		 * Use the comic book
		 * @param	target		Player using it
		 */
		override public function Use(subject:Player, object:Monster):void 
		{
			GameWorld.log.AddLine( EFFECT_LOGS[comicType] );
			if ( !identified[comicType] )
				IdentifyComic( comicType );
			
			switch( comicType )
			{
				case FIRE_BLAST:
					fireblastSfx.play( 0.2 );
					FireBlast( subject );
					break;
				case MEGA_HEAL:
					megahealSfx.play( 0.2 );
					subject.Heal( MEGA_HEAL_AMOUNT );
					break;
				case LIGHTNING:
					lightningSfx.play( 0.2 );
					Lightning( subject );
					break;
				case LAND_MINE:
					subject.world.add( new LandMine( subject.row, subject.column ) );
					break;
				case TELEPORT:
					teleportSfx.play( 0.2 );
					subject.FindAvailableSpace();
					break;
				case REVEAL:
					GameWorld.currentFloor.ClearMap();
					break;
				case GENOCIDE:
					subject.world.add( new DeathFloater( subject.x, subject.y - subject.height ) );
					Genocide( subject );
					break;
				case FREEZE:
					freezeSfx.play( 0.2 );
					ShootIce( subject );
					break;
				case CONFUSION:
					confusionSfx.play( 0.2 );
					subject.Confuse( 10 );
					break;
				case POISON_GAS:
					poisonGasSfx.play( 0.2 );
					PoisonGas( subject );
					break;
			}
		}
		
		/**
		 * Update all descriptions on existing comics of a newly identified type
		 */
		protected function IdentifyComic( type:int ):void
		{
			for each( var item:Item in GameWorld.items )
			{
				if ( item is Comic && (item as Comic).comicType == type )
				{
					item.description += EFFECT_DESCS[type];
				}
			}
			
			identified[comicType] = true;
		}
		
		protected function Genocide( player:Player ):void
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			player.world.getClass( Monster, monsters );
			if ( monsters.length == 0 )
				return;
			
			var targetMonster:Class = Class( getDefinitionByName( getQualifiedClassName( monsters[FP.rand( monsters.length )] ) ) );
			for each( var monster:Monster in monsters )
			{
				if ( monster is targetMonster )
					monster.Die();
			}
		}
		
		protected function FireBlast( player:Player ):void
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			player.world.getClass( Monster, monsters );
			
			for (var row:int = player.row - 2; row <= player.row + 2; ++row) 
			{
				for (var column:int = player.column - 2; column <= player.column + 2; ++column) 
				{
					if ( row != player.row || column != player.column )
					{
						var monster:Monster =  CheckMonster( row, column, monsters );
						if ( monster != null )
							monster.Hurt( 30 + FP.rand( 10 ) );
						player.world.add( new Flame( row, column ) );
					}
				}
			}
		}
		
		protected function PoisonGas( player:Player ):void
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			player.world.getClass( Monster, monsters );
			
			player.Hurt( 5 + FP.rand( 5 ) );
			for (var row:int = player.row - 2; row <= player.row + 2; ++row) 
			{
				for (var column:int = player.column - 2; column <= player.column + 2; ++column) 
				{
					var monster:Monster =  CheckMonster( row, column, monsters );
					if ( monster != null )
					{
						monster.Hurt( 30 + FP.rand( 10 ) );
					}
					player.world.add( new GasCloud( row, column ) );
				}
			}
		}
		
		protected function Lightning( player:Player ):void
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			player.world.getClass( Monster, monsters );
			
			var rowStep:int;
			var colStep:int;
			
			switch( player.facing )
			{
				case Player.FACING_UP:
					rowStep = -1;
					colStep = 0;
					break;
				case Player.FACING_LEFT:
					rowStep = 0;
					colStep = -1;
					break;
				case Player.FACING_DOWN:
					rowStep = 1;
					colStep = 0;
					break;
				case Player.FACING_RIGHT:
					rowStep = 0;
					colStep = 1;
					break;
			}
			
			var row:int		= player.row;
			var column:int	= player.column;
			for (var i:int = 1; i < 4; i++) 
			{
				row += rowStep;
				column += colStep;
				var monster:Monster = CheckMonster( row, column, monsters );
				if ( monster != null )
					monster.Hurt( 30 + FP.rand( 10 ) );
				player.world.add( new LightningBolt( row, column, rowStep != 0 ) );
			}
		}
		
		protected function ShootIce( player:Player ):void
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			player.world.getClass( Monster, monsters );
			
			var rowStep:int;
			var colStep:int;
			
			switch( player.facing )
			{
				case Player.FACING_UP:
					rowStep = -1;
					colStep = 0;
					break;
				case Player.FACING_LEFT:
					rowStep = 0;
					colStep = -1;
					break;
				case Player.FACING_DOWN:
					rowStep = 1;
					colStep = 0;
					break;
				case Player.FACING_RIGHT:
					rowStep = 0;
					colStep = 1;
					break;
			}
			
			var row:int		= player.row;
			var column:int	= player.column;
			for (var i:int = 1; i < 4; i++) 
			{
				row += rowStep;
				column += colStep;
				var monster:Monster = CheckMonster( row, column, monsters );
				if ( monster != null )
				{
					monster.Hurt( 30 + FP.rand( 10 ) );
					monster.Freeze( 5 );
				}
				player.world.add( new IceBolt( row, column ) );
			}
		}
		
		protected function CheckMonster( row:int, column:int, monsters:Vector.<Monster> ):Monster
		{
			for each( var monster:Monster in monsters )
			{
				if ( monster.row == row && monster.column == column )
					return monster;
			}
			
			return null;
		}
	}

}