package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	import Sound.RandomSound;
	
	/**
	 * Energy drink item
	 * @author Switchbreak
	 */
	public class EnergyDrink extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/e-drink.png")]     protected static const ENERGY_DRINK:Class;
		[Embed(source = "../../snd/Items/energydrink.mp3")] protected static const ENERGY_DRINK_SND:Class;
		
		// Constants
		protected static const NAME:String			= "Energy Drink";
		protected static const DESCRIPTION:String	= "Just the pick up you need! Restores about 20 HP.";
		protected static const SPRITE_WIDTH:int		= 64;
		protected static const SPRITE_HEIGHT:int	= 64;
		protected static const HEAL_AMOUNT:int		= 40;
		protected static const HEAL_RANGE:int		= 5;
		
		// Sounds
		protected var energyDrinkSound:RandomSound = new RandomSound(
		    [ENERGY_DRINK_SND],
		    0.7);

		/**
		 * Create and initialize the energy drink
		 */
		public function EnergyDrink() 
		{
			name		= NAME;
			description	= DESCRIPTION;
			graphic		= new Stamp( ENERGY_DRINK );
			usable		= true;
			throwable	= true;
			equipable	= Item.EQUIPABLE_NOT;
			wearable	= false;
			active		= false;
			width		= SPRITE_WIDTH;
			height		= SPRITE_HEIGHT;
			value		= 5;
		}
		
		/**
		 * Heal player when drunk
		 */
		override public function Use(subject:Player, object:Monster):void 
		{
			subject.Heal( HEAL_AMOUNT - HEAL_RANGE + FP.rand( HEAL_RANGE << 1 ) );
			energyDrinkSound.play();
		}
	}
}