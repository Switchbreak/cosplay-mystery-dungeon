package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	
	/**
	 * Money game item
	 * @author Switchbreak
	 */
	public class Money extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/money.png")]     protected static const MONEY:Class;
		
		// Constants
		protected static const SPRITE_WIDTH:int		= 64;
		protected static const SPRITE_HEIGHT:int	= 64;
		protected static const MONEY_AMOUNT:Number	= 5.00;
		protected static const MONEY_RANGE:Number	= 2.00;
		
		/**
		 * Create and initialize the item
		 */
		public function Money() 
		{
			value = GameWorld.floorNumber + MONEY_AMOUNT - MONEY_RANGE + FP.random * MONEY_RANGE * 2;
			value = Math.round( value * 100 ) / 100;
			
			name		= "$" + value.toFixed( 2 );
			description	= name;
			graphic		= new Stamp( MONEY );
			usable		= false;
			throwable	= false;
			equipable	= Item.EQUIPABLE_NOT;
			wearable	= false;
			active		= false;
			width		= SPRITE_WIDTH;
			height		= SPRITE_HEIGHT;
		}
		
		/**
		 * When picked up, immediately remove self from inventory and add to
		 * the player's money counter
		 * @param	target		Reference to player that picked up the money
		 */
		override public function PickUp(target:Player):void 
		{
			target.money += value;
			
			target.DropItem( this );
			GameWorld.items.splice( GameWorld.items.indexOf( this ), 1 );
		}
	}
}