package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	
	/**
	 * Ribbon game item
	 * @author Switchbreak
	 */
	public class Ribbon extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/ribbon.png")]		private static const RIBBON:Class;
		
		/**
		 * Create and initialize the item
		 */
		public function Ribbon() 
		{
			defense			= 1 + FP.rand( 4 );
			
			name			= "Ribbon";
			description		= "Tie it on for an extra +" + defense.toString() + " defense.";
			graphic			= new Stamp( RIBBON );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_NOT;
			wearable		= true;
			active			= false;
			width			= 64;
			height			= 64;
			value			= 5;
		}
	}
}