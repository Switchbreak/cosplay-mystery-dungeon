package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	
	/**
	 * Ribbon game item
	 * @author Switchbreak
	 */
	public class Ribbon2 extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/ribbon2.png")]		private static const RIBBON:Class;
		
		/**
		 * Create and initialize the item
		 */
		public function Ribbon2() 
		{
			defense			= 5 + FP.rand( 6 );
			
			name			= "Ribbon Level 2";
			description		= "Tie it on for an extra +" + defense.toString() + " defense.";
			graphic			= new Stamp( RIBBON );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_NOT;
			wearable		= true;
			active			= false;
			width			= 64;
			height			= 64;
			value			= 5;
		}
	}
}