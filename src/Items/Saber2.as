package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	import Types.Item;
	
	/**
	 * Saber game item
	 * @author Switchbreak
	 */
	public class Saber2 extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/saber2.png")]		private static const SABER:Class;
		
		[Embed(source = "../../snd/Weapons/saber1.mp3")] 	private static const SABER_SND1:Class;
		[Embed(source = "../../snd/Weapons/saber2.mp3")] 	private static const SABER_SND2:Class;
		[Embed(source = "../../snd/Weapons/saber3.mp3")] 	private static const SABER_SND3:Class;
		[Embed(source = "../../snd/Weapons/saber4.mp3")] 	private static const SABER_SND4:Class;
		
		protected var saberSound:RandomSound = new RandomSound(
		    [SABER_SND1, SABER_SND2, SABER_SND3, SABER_SND4],
		    2.0);
		
		/**
		 * Create and initialize the item
		 */
		public function Saber2() 
		{
			name			= "Saber Level 2";
			description		= "It's some kind of saber made of... illumination.";
			graphic			= new Stamp( SABER );
			usable			= false;
			throwable		= true;
			equipable		= Item.EQUIPABLE_DOUBLE_HANDED;
			wearable		= false;
			active			= false;
			width			= 64;
			height			= 64;
			attack			= 40;
			value			= 40;
		}
		
		override public function Use(subject:Player, object:Monster):void 
		{
			saberSound.play();
			
			super.Use(subject, object);
		}
	}
}