package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	import Types.Item;
	
	/**
	 * Sword game item
	 * @author Switchbreak
	 */
	public class Sword2 extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/sword2.png")]	protected static const SWORD:Class;
		
		[Embed(source = "../../snd/Weapons/sword1.mp3")] 	private static const SWORD_SND1:Class;
		[Embed(source = "../../snd/Weapons/sword2.mp3")] 	private static const SWORD_SND2:Class;
		[Embed(source = "../../snd/Weapons/sword3.mp3")] 	private static const SWORD_SND3:Class;
		[Embed(source = "../../snd/Weapons/sword4.mp3")] 	private static const SWORD_SND4:Class;
		    
		static protected var swordSound:RandomSound = new RandomSound(
		    [SWORD_SND1, SWORD_SND2, SWORD_SND3, SWORD_SND4],
		    0.5);
		
		/**
		 * Create and initialize the item
		 */
		public function Sword2() 
		{
			name			= "Sword Level 2";
			description		= "Found at the medieval armory booth.";
			graphic			= new Stamp( SWORD );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_DOUBLE_HANDED;
			wearable		= false;
			active			= false;
			width			= 64;
			height			= 64;
			attack			= 25;
			value			= 25;
		}
		
		/**
		 * Play sword sound when attacking
		 */
		override public function Use(subject:Player, object:Monster):void 
		{
			swordSound.play();
			
			super.Use(subject, object);
		}
	}
}