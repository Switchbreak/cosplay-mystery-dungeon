package Items 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	import Types.Item;
	
	/**
	 * Wand game item
	 * @author Switchbreak
	 */
	public class Wand extends Item 
	{
		// Assets
		[Embed(source = "../../img/Items/wandheart.png")]	private static const WAND:Class;
		
		[Embed(source = "../../snd/Weapons/wand1.mp3")] 	private static const WAND_SND1:Class;
		[Embed(source = "../../snd/Weapons/wand2.mp3")] 	private static const WAND_SND2:Class;
		[Embed(source = "../../snd/Weapons/wand3.mp3")] 	private static const WAND_SND3:Class;
		[Embed(source = "../../snd/Weapons/wand4.mp3")] 	private static const WAND_SND4:Class;
		
		static protected var wandSound:RandomSound = new RandomSound(
		    [WAND_SND1, WAND_SND2, WAND_SND3, WAND_SND4],
		    0.2);
		
		/**
		 * Create and initialize the item
		 */
		public function Wand() 
		{
			name			= "Wand";
			description		= "Fires beams of magic energy.";
			graphic			= new Stamp( WAND );
			usable			= false;
			throwable		= true;
			equipable		= EQUIPABLE_SINGLE_HANDED;
			wearable		= false;
			active			= false;
			width			= 64;
			height			= 64;
			attack			= 15;
			value			= 15;
		}
		
		override public function Use(subject:Player, object:Monster):void 
		{
			wandSound.play();
			
			super.Use(subject, object);
		}
	}
}