package  
{
	import net.flashpunk.FP;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import UI.Button;
	import UI.Fader;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class LoseScreen extends World 
	{
		// Assets
		[Embed(source = "../img/Menus/youlosescreen.png")]	protected static const	LOSE_SCREEN:Class;
		[Embed(source="../img/Menus/retrybutton.png")]		protected static const	RETRY:Class;
		[Embed(source="../snd/fail_cue.mp3")]               protected static const  FAIL_MUSIC_SND:Class;
		
		// Constants
		protected static const	BUTTON_WIDTH:int	= 104;
		protected static const	BUTTON_HEIGHT:int	= 32;
		protected static const	BUTTON_X:int		= FP.halfWidth - 52;
		protected static const	BUTTON_Y:int		= FP.height * 3 / 4;
		
		protected var fader:Fader;
		
		// Sounds
		protected var failMusic:Sfx = new Sfx(FAIL_MUSIC_SND);
		
		public function LoseScreen() 
		{
			addGraphic( new Stamp( LOSE_SCREEN ) );
			
			var startButton:Button = new Button( RETRY, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_X, BUTTON_Y, false, FadeOut );
			addGraphic( startButton );
			
			fader = new Fader();
			add( fader );
			
			failMusic.play(0.8);
		}
		
		public function FadeOut():void
		{
			fader.Start( true, 0.5, StartGame );
		}
		
		public function StartGame():void 
		{
			FP.world = new GameWorld( null, null, GameWorld.floorNumber );
		}
	}
}