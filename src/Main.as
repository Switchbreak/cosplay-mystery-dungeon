package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	
	/**
	 * Main class to initialize engine and game world object
	 * @author Switchbreak
	 */
	public class Main extends Engine 
	{
		// Constants
		static private const ENGINE_WIDTH:uint	= 640;
		static private const ENGINE_HEIGHT:uint	= 480;
		
		/**
		 * Application entry point
		 */
		public function Main():void 
		{
			super( ENGINE_WIDTH, ENGINE_HEIGHT );
		}
		
		/**
		 * Function called after engine finishes initializing to create game
		 * world object
		 */
		override public function init():void 
		{
			FP.world = new MainMenu();
		}
		
	}
	
}