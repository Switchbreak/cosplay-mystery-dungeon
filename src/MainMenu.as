package  
{
	import net.flashpunk.FP;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Stamp;
	import UI.Button;
	import UI.Fader;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class MainMenu extends World 
	{
		// Assets
		[Embed(source = "../img/Menus/logoscreen.png")]		protected static const	LOGO:Class;
		[Embed(source = "../img/Menus/storycard.png")]		protected static const	STORY_CARD:Class;
		[Embed(source = "../img/Menus/startbutton.png")]	protected static const	START:Class;
		
		// Constants
		protected static const	BUTTON_WIDTH:int	= 104;
		protected static const	BUTTON_HEIGHT:int	= 32;
		protected static const	BUTTON_X:int		= FP.halfWidth - 52;
		protected static const	BUTTON_Y:int		= FP.height * 3 / 4;
		
		protected var fader:Fader;
		protected var storyCard:Stamp;
		protected var startButton:Button;
		
		public function MainMenu() 
		{
			addGraphic( new Stamp( LOGO ) );
			
			startButton = new Button( START, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_X, BUTTON_Y, false, FadeOut );
			addGraphic( startButton );
			
			storyCard = new Stamp( STORY_CARD );
			storyCard.visible = false;
			addGraphic( storyCard );
			
			fader = new Fader();
			add( fader );
		}
		
		override public function update():void 
		{
			if ( Input.pressed( Key.ANY ) || Input.mousePressed )
			{
				if( storyCard.visible )
					FadeOutStory();
				else
					FadeOut();
			}
			
			super.update();
		}
		
		public function FadeOut():void
		{
			startButton.active = false;
			fader.Start( true, 0.5, StoryCard );
		}
		
		public function StoryCard():void
		{
			storyCard.visible = true;
			fader.Start( false, 0.5, null );
		}
		
		public function FadeOutStory():void
		{
			fader.Start( true, 0.5, StartGame );
		}
		
		public function StartGame():void 
		{
			FP.world = new GameWorld();
		}
	}
}