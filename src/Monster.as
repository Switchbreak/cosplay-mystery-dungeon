package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import mx.utils.StringUtil;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.motion.LinearMotion;
	import UI.Floater;
	
	/**
	 * Monster generic game object
	 * @author Switchbreak
	 */
	public class Monster extends Entity 
	{
		// Assets
		[Embed(source = "../img/Shadow.png")]	private static const SHADOW:Class;
		
		// Constants
		protected static const	GRAPHIC_WIDTH:int		= 64;
		protected static const	GRAPHIC_HEIGHT:int		= 64;
		protected static const	ATTACK_LOG:String		= "{0} attacks!";
		protected static const	DEATH_LOG:String		= "{0} has died.";
		protected static const	WALK_RATE:Number		= 0.4;
		protected static const	ATTACK_RATE:Number		= 0.3;
		protected static const	DEATH_DURATION:Number	= 0.4;
		
		// Directions
		protected static const	FACING_DOWN:int		= 0;
		protected static const	FACING_LEFT:int		= 1;
		protected static const	FACING_RIGHT:int	= 2;
		protected static const	FACING_UP:int		= 3;
		
		// Animations
		protected static const	ANIM_IDLE_NAMES:Array		= ["Idle Down", "Idle Left", "Idle Right", "Idle Up"]
		protected static const	ANIM_IDLE_FRAMES:Array		= [[0], [4], [4], [8]];
		protected static const	ANIM_WALK_NAMES:Array		= ["Walk Down", "Walk Left", "Walk Right", "Walk Up"]
		protected static const	ANIM_WALK_FRAMES:Array		= [[0, 1, 2, 3], [4, 5, 6, 7], [4, 5, 6, 7], [8, 9, 10, 11]];
		protected static const	ANIM_ATTACK_NAMES:Array		= ["Attack Down", "Attack Left", "Attack Right", "Attack Up"]
		protected static const	ANIM_ATTACK_FRAMES:Array	= [[0, 12, 0], [4, 13, 4], [4, 13, 4], [8, 15, 8]];
		protected static const	ANIM_HURT_NAMES:Array		= ["Hurt Down", "Hurt Left", "Hurt Right", "Hurt Up"]
		protected static const	ANIM_HURT_FRAMES:Array		= [[0, 16, 0], [4, 17, 4], [4, 17, 4], [8, 19, 8]];
		protected static const	ANIM_DIE_NAMES:Array		= ["Die Down", "Die Left", "Die Right", "Die Up"]
		protected static const	ANIM_DIE_FRAMES:Array		= [[16, 17, 19, 20], [17, 19, 20], [17, 16, 18, 19, 20], [19, 18, 16, 17, 19, 20]];
		protected static const	ANIM_RATE:Number			= 10;
		
		// Variables
		public var		row:int;
		public var		column:int;
		public var		HP:int;
		public var		might:int;
		public var		resilience:int;
		public var		monsterName:String;
		public var		facing:int;
		public var		dead:Boolean;
		public var		frozen:Boolean;
		public var		freezeTimer:int;
		protected var	monsterMotion:LinearMotion;
		protected var	monsterSprite:Spritemap;
		
		/**
		 * Create and initialize the monster object
		 * @param	setGraphic		Monster graphic
		 * @param	setRow			Starting position
		 * @param	setColumn		Starting position
		 */
		public function Monster( setGraphic:Class, setRow:int, setColumn:int )
		{
			super();
			
			monsterSprite = new Spritemap( setGraphic, GRAPHIC_WIDTH, GRAPHIC_HEIGHT );
			monsterSprite.originX = monsterSprite.width >> 1;
			monsterSprite.originY = monsterSprite.height;
			
			for (var i:int = 0; i < 4; i++) 
			{
				monsterSprite.add( ANIM_IDLE_NAMES[i], ANIM_IDLE_FRAMES[i], ANIM_RATE, false );
				monsterSprite.add( ANIM_WALK_NAMES[i], ANIM_WALK_FRAMES[i], ANIM_RATE, true );
				monsterSprite.add( ANIM_ATTACK_NAMES[i], ANIM_ATTACK_FRAMES[i], ANIM_RATE, false );
				monsterSprite.add( ANIM_HURT_NAMES[i], ANIM_HURT_FRAMES[i], ANIM_RATE, false );
				monsterSprite.add( ANIM_DIE_NAMES[i], ANIM_DIE_FRAMES[i], ANIM_RATE, false );
			}
			
			var shadow:Stamp = new Stamp( SHADOW );
			shadow.x = -(shadow.width >> 1);
			shadow.y = -(shadow.height >> 1);
			
			setHitboxTo( monsterSprite );
			
			layer			= -1 - row;
			row				= setRow;
			column			= setColumn;
			graphic			= new Graphiclist( shadow, monsterSprite );
			dead			= false;
			
			monsterMotion	= new LinearMotion( Arrive, Tween.PERSIST );
			addTween( monsterMotion );
			
			x = FloorMap.TILE_WIDTH * column + (FloorMap.TILE_WIDTH >> 1);
			y = FloorMap.TILE_HEIGHT * row + (FloorMap.TILE_HEIGHT >> 1);
		}
		
		/**
		 * Handle motion
		 */
		override public function update():void 
		{
			if ( monsterMotion.active )
				FollowMotion();
			
			super.update();
		}
		
		/**
		 * Called every turn, override to add behavior
		 */
		public function TakeTurn():void
		{
			if ( frozen )
			{
				if ( --freezeTimer <= 0 )
				{
					frozen = false;
					GameWorld.log.AddLine( monsterName + " has thawed out!" );
				}
			}
		}
		
		public function Freeze( duration:int ):void
		{
			frozen = true;
			freezeTimer = duration;
			GameWorld.log.AddLine( monsterName + " is frozen in place!" );
		}
		
		/**
		 * Called when monster is hurt, decreases HP and checks for death
		 * @param	damage		Amount of damage to hurt
		 */
		public function Hurt( damage:int ):void
		{
			damage -= resilience;
			if ( damage < 0 )
				damage = 0;
			
			HP -= damage;
			
			monsterSprite.play( ANIM_HURT_NAMES[facing], true );
			world.add( new Floater( "-" + damage.toString(), x, y - height, 0xFFFF00 ) );
			
			if ( HP <= 0 )
				Die();
		}
		
		/**
		 * Called when monster has died
		 */
		public function Die():void 
		{
			dead = true;
			(world as GameWorld).WaitOnAction();
			monsterSprite.play( ANIM_DIE_NAMES[facing], true );
			addTween( new Alarm( DEATH_DURATION, Dead, Tween.ONESHOT ), true );
		}
		
		protected function Dead():void
		{
			(world as GameWorld).EndTurn();
			GameWorld.log.AddLine( StringUtil.substitute( DEATH_LOG, monsterName ) );
			world.remove( this );
		}
		
		/**
		 * Attack the player
		 */
		public function Attack():void
		{
			GameWorld.log.AddLine( StringUtil.substitute( ATTACK_LOG, monsterName ) );
			SetFacing( GameWorld.player.row, GameWorld.player.column );
			monsterSprite.play( ANIM_ATTACK_NAMES[facing], true );
			GameWorld.player.Hurt( 5 + might - 5 + FP.rand( 10 ) );
			(world as GameWorld).WaitOnAction();
			addTween( new Alarm( ATTACK_RATE, (world as GameWorld).EndTurn, Tween.ONESHOT ), true );
		}
		
		/**
		 * Wander aimlessly
		 */
		public function Wander():void
		{
			var step:int = (FP.random > 0.5 ? 1 : -1);
			
			if( FP.random > 0.5 )
				MoveTo( row + step, column );
			else 
				MoveTo( row, column + step );
		}
		
		/**
		 * Move one space toward the player
		 */
		public function ChasePlayer():void
		{
			var deltaRow:int = GameWorld.player.row - row;
			var deltaColumn:int = GameWorld.player.column - column;
			var rowStep:int = (deltaRow > 0 ? 1 : -1);
			var colStep:int = (deltaColumn > 0 ? 1 : -1);
			
			if ( !CheckLocation( row + rowStep, column ) )
				deltaRow = 0;
			if ( !CheckLocation( row, column + colStep ) )
				deltaColumn = 0;
			
			if ( Math.abs( deltaRow ) > Math.abs( deltaColumn ) )
				MoveTo( row + rowStep, column );
			else
				MoveTo( row, column + colStep );
		}
		
		/**
		 * Move one space toward the player
		 */
		public function FleePlayer():void
		{
			var deltaRow:int = GameWorld.player.row - row;
			var deltaColumn:int = GameWorld.player.column - column;
			var rowStep:int = (deltaRow > 0 ? 1 : -1);
			var colStep:int = (deltaColumn > 0 ? 1 : -1);
			
			if ( !CheckLocation( row - rowStep, column ) )
				deltaRow = 100;
			if ( !CheckLocation( row, column - colStep ) )
				deltaColumn = 100;
			
			if ( Math.abs( deltaRow ) < Math.abs( deltaColumn ) )
				MoveTo( row - rowStep, column );
			else
				MoveTo( row, column - colStep );
		}
		
		/**
		 * Move to a destination and check for collisions
		 * @param	destRow		Destination map coordinates
		 * @param	destColumn	Destination map coordinates
		 */
		public function MoveTo( destRow:int, destColumn:int ):void 
		{
			if ( frozen )
				return;
			
			if ( row == destRow && column == destColumn )
				return;
			
			if ( destRow < 0 || destRow >= FloorMap.MAP_HEIGHT )
				return;
			if ( destColumn < 0 || destColumn >= FloorMap.MAP_WIDTH )
				return;
			
			if ( !CheckLocation( destRow, destColumn ) )
				return;
			
			SetFacing( destRow, destColumn );
			
			if ( CheckCollisions( destRow, destColumn ) )
				return;
			
			row		= destRow;
			column	= destColumn;
			layer	= -1 - row;
			
			monsterSprite.play( ANIM_WALK_NAMES[facing], true );
			monsterMotion.setMotion(	x, y,
										FloorMap.TILE_WIDTH * column + (FloorMap.TILE_WIDTH >> 1),
										FloorMap.TILE_HEIGHT * row + (FloorMap.TILE_HEIGHT >> 1),
										WALK_RATE );
		}
		
		/**
		 * Set the facing toward a direction
		 * @param	destRow		
		 * @param	destColumn
		 */
		protected function SetFacing( destRow:int, destColumn:int ):void 
		{
			monsterSprite.flipped = false;
			
			if ( destRow < row )
				facing = FACING_UP;
			else if ( destRow > row )
				facing = FACING_DOWN;
			else if ( destColumn < column )
				facing = FACING_LEFT;
			else
			{
				facing = FACING_RIGHT;
				monsterSprite.flipped = true;
			}
		}
		
		/**
		 * Check if a location is walkable
		 * @param	destRow		Location to check
		 * @param	destColumn	Location to check
		 * @return	True if location is walkable, else false
		 */
		protected function CheckLocation( destRow:int, destColumn:int ):Boolean
		{
			return (GameWorld.currentFloor.GetTileAtLocation( destRow, destColumn ) == FloorMap.GAME_TILE_FLOOR);
		}
		
		/**
		 * Move monster position to the motion tween position
		 */
		protected function FollowMotion():void
		{
			x = monsterMotion.x;
			y = monsterMotion.y;
		}
		
		/**
		 * Motion is completed
		 */
		protected function Arrive():void 
		{
			monsterSprite.play( ANIM_IDLE_NAMES[facing], true );
			FollowMotion();
		}
		
		/**
		 * Check for a collision with player or other monsters
		 * @param	destRow		Position to check
		 * @param	destColumn	Position to check
		 * @return	True if collision was found, false if space is clear
		 */
		protected function CheckCollisions( destRow:int, destColumn:int ):Boolean 
		{
			if ( GameWorld.player.row == destRow && GameWorld.player.column == destColumn )
				return true;
			
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			world.getClass( Monster, monsters );
			
			for each( var monster:Monster in monsters )
			{
				if ( monster.row == destRow && monster.column == destColumn )
					return true;
			}
			
			var objects:Vector.<GameObject> = new Vector.<GameObject>();
			world.getClass( GameObject, objects );
			
			for each( var object:GameObject in objects )
			{
				if ( object.row == destRow && object.column == destColumn )
				{
					object.Activate( this );
					return object.blocking;
				}
			}
			
			return false;
		}
		
		/**
		 * Checks if the player is adjacent to the monster
		 * @return	True if player is adjacent, false if not
		 */
		protected function PlayerAdjacent():Boolean
		{
			return (Math.abs( GameWorld.player.row - row ) + Math.abs( GameWorld.player.column - column ) == 1);
		}
		
		/**
		 * Checks if the monster has line of sight to the player
		 * @return	True if monster has line of sight, false if not
		 */
		protected function PlayerVisible():Boolean
		{
			return GameWorld.currentFloor.LineOfSight( GameWorld.player.row, GameWorld.player.column, row, column );
		}
		
		/**
		 * Checks if the player is within a given range of the monster
		 * @return	True if monster has line of sight, false if not
		 */
		protected function PlayerInRange( distance:int ):Boolean
		{
			var deltaRow:int = GameWorld.player.row - row;
			var deltaColumn:int = GameWorld.player.column - column;
			
			return (deltaRow * deltaRow + deltaColumn * deltaColumn <= distance * distance);
		}
	}
}