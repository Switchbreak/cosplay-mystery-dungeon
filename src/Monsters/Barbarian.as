package Monsters 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import Sound.RandomSound;
	import UI.Floater;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Barbarian extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/barbarian-sheet.png")]		private static const BARBARIAN:Class;
		
		[Embed(source = "../../snd/Weapons/sword1.mp3")] 	private static const SWORD_SND1:Class;
		[Embed(source = "../../snd/Weapons/sword2.mp3")] 	private static const SWORD_SND2:Class;
		[Embed(source = "../../snd/Weapons/sword3.mp3")] 	private static const SWORD_SND3:Class;
		[Embed(source = "../../snd/Weapons/sword4.mp3")] 	private static const SWORD_SND4:Class;
		
		// Constants
		protected static const	BARBARIAN_WIDTH:int	= 64;
		protected static const	BARBARIAN_HEIGHT:int	= 64;
		protected static const	NAME:String				= "Barbarian";
		protected static const	STARTING_HP:int			= 100;
		protected static const	TRIGGER_RANGE:int		= 4;
		protected static const	FLEE_HP:int				= 10;
		protected static const	HOSTILE_LOG:String		= "A Barbarian is hunting you!";
		protected static const	FLEE_LOG:String			= "A Barbarian is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int		= 0;
		protected static const	STATE_HOSTILE:int		= 1;
		protected static const	STATE_FLEEING:int		= 2;
		
		// Variables
		protected var	state:int;
		static protected var swordSound:RandomSound = new RandomSound(
		    [SWORD_SND1, SWORD_SND2, SWORD_SND3, SWORD_SND4],
		    0.5);
		
		/**
		 * Create and initialize the Barbarian
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function Barbarian( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 20;
			resilience	= 0;
			
			super( BARBARIAN, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerAdjacent() )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			swordSound.play();
			
			super.Attack();
		}
		
		/**
		 * Drop an item on death
		 */
		override protected function Dead():void
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}