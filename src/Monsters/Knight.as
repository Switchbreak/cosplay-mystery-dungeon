package Monsters 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import Sound.RandomSound;
	import UI.Floater;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Knight extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/fencer-male-sheet2.png")]	private static const KNIGHT:Class;
		
		[Embed(source = "../../snd/Weapons/saber1.mp3")] 	private static const SABER_SND1:Class;
		[Embed(source = "../../snd/Weapons/saber2.mp3")] 	private static const SABER_SND2:Class;
		[Embed(source = "../../snd/Weapons/saber3.mp3")] 	private static const SABER_SND3:Class;
		[Embed(source = "../../snd/Weapons/saber4.mp3")] 	private static const SABER_SND4:Class;
		
		// Constants
		protected static const	KNIGHT_WIDTH:int	= 64;
		protected static const	KNIGHT_HEIGHT:int	= 64;
		protected static const	NAME:String			= "Knight";
		protected static const	STARTING_HP:int		= 150;
		protected static const	TRIGGER_RANGE:int	= 4;
		protected static const	FLEE_HP:int			= 10;
		protected static const	HOSTILE_LOG:String	= "A Knight is hunting you!";
		protected static const	FLEE_LOG:String		= "A Knight is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int	= 0;
		protected static const	STATE_HOSTILE:int	= 1;
		protected static const	STATE_FLEEING:int	= 2;
		
		// Variables
		protected var	state:int;
		static protected var	saberSound:RandomSound = new RandomSound(
		    [SABER_SND1, SABER_SND2, SABER_SND3, SABER_SND4],
		    2.0);
		
		/**
		 * Create and initialize the Knight
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function Knight( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 35;
			resilience	= 0;
			
			super( KNIGHT, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerAdjacent() )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			saberSound.play();
			super.Attack();
		}
		
		/**
		 * Drop an item on death
		 */
		override protected function Dead():void
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}