package Monsters 
{
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import Sound.RandomSound;
	
	/**
	 * Kung Fu Fighter monster type
	 * @author Switchbreak
	 */
	public class KungFuFighter extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/kungfu-sheet.png")]	private static const KUNG_FU:Class;
		
		[Embed(source = "../../snd/Weapons/fists1.mp3")] 		private static const FISTS_SND1:Class;
		[Embed(source = "../../snd/Weapons/fists2.mp3")] 		private static const FISTS_SND2:Class;
		[Embed(source = "../../snd/Weapons/fists3.mp3")] 		private static const FISTS_SND3:Class;
		[Embed(source = "../../snd/Weapons/fists4.mp3")] 		private static const FISTS_SND4:Class;
		
		
		// Constants
		protected static const	NAME:String				= "Kung Fu Fighter";
		protected static const	STARTING_HP:int			= 50;
		protected static const	TRIGGER_RANGE:int		= 4;
		protected static const	FLEE_HP:int				= 0;
		protected static const	HOSTILE_LOG:String		= "A Kung Fu Fighter is hunting you!";
		protected static const	FLEE_LOG:String			= "A Kung Fu Fighter is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int			= 0;
		protected static const	STATE_HOSTILE:int			= 1;
		protected static const	STATE_FLEEING:int			= 2;
		
		// Variables
		protected var	state:int;
		static protected var	fistsSound:RandomSound = new RandomSound(
		    [FISTS_SND1, FISTS_SND2, FISTS_SND3, FISTS_SND4],
		    0.4);
		
		/**
		 * Create and initialize the kung fu fighter
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function KungFuFighter( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 5;
			resilience	= 0;
			
			super( KUNG_FU, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerAdjacent() )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			fistsSound.play();
			
			super.Attack();
		}
		
		/**
		 * Drop a random item when monster dies
		 */
		override protected function Dead():void 
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}