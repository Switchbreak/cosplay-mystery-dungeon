package Monsters 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import UI.Floater;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Robot extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/soldier-female-sheet.png")]	private static const ROBOT:Class;
		[Embed(source = "../../snd/Weapons/blaster.mp3")]				private static const BLASTER_SND:Class;
		
		// Constants
		protected static const	ROBOT_WIDTH:int			= 64;
		protected static const	ROBOT_HEIGHT:int		= 64;
		protected static const	NAME:String				= "Terminator";
		protected static const	STARTING_HP:int			= 100;
		protected static const	TRIGGER_RANGE:int		= 4;
		protected static const	FLEE_HP:int				= 0;
		protected static const	HOSTILE_LOG:String		= "A Terminator is hunting you!";
		protected static const	FLEE_LOG:String			= "A Terminator is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int			= 0;
		protected static const	STATE_HOSTILE:int			= 1;
		protected static const	STATE_FLEEING:int			= 2;
		
		// Variables
		protected var	state:int;
		static protected var	blasterSfx:Sfx = new Sfx( BLASTER_SND );
		
		/**
		 * Create and initialize the Stormtrooper
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function Robot( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 10;
			resilience	= 0;
			
			super( ROBOT, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerInRange( 2 ) )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			blasterSfx.play( 0.2 );
			super.Attack();
			world.add( new Bullet( x, y - 32, GameWorld.player.x, GameWorld.player.y - 32 ) );
		}
		
		/**
		 * Drop an item on death
		 */
		override protected function Dead():void
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}