package Monsters 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import Sound.RandomSound;
	import UI.Floater;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Scout extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/scout-sheet.png")]	private static const SCOUT:Class;
		
		[Embed(source = "../../snd/Weapons/wand1.mp3")] 	private static const WAND_SND1:Class;
		[Embed(source = "../../snd/Weapons/wand2.mp3")] 	private static const WAND_SND2:Class;
		[Embed(source = "../../snd/Weapons/wand3.mp3")] 	private static const WAND_SND3:Class;
		[Embed(source = "../../snd/Weapons/wand4.mp3")] 	private static const WAND_SND4:Class;
		
		// Constants
		protected static const	SCOUT_WIDTH:int			= 64;
		protected static const	SCOUT_HEIGHT:int		= 64;
		protected static const	NAME:String				= "Scout";
		protected static const	STARTING_HP:int			= 150;
		protected static const	TRIGGER_RANGE:int		= 4;
		protected static const	FLEE_HP:int				= 0;
		protected static const	HOSTILE_LOG:String		= "A Scout is hunting you!";
		protected static const	FLEE_LOG:String			= "A Scout is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int			= 0;
		protected static const	STATE_HOSTILE:int			= 1;
		protected static const	STATE_FLEEING:int			= 2;
		
		// Variables
		protected var	state:int;
		static protected var	wandSound:RandomSound = new RandomSound(
		    [WAND_SND1, WAND_SND2, WAND_SND3, WAND_SND4],
		    0.2);
		
		/**
		 * Create and initialize the Stormtrooper
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function Scout( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 25;
			resilience	= 0;
			
			super( SCOUT, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerInRange( 2 ) )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			wandSound.play();
			super.Attack();
			world.add( new Bullet( x, y - 32, GameWorld.player.x, GameWorld.player.y - 32, true ) );
		}
		
		/**
		 * Drop an item on death
		 */
		override protected function Dead():void
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}