package Monsters 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import UI.Floater;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Vampire extends Monster 
	{
		// Assets
		[Embed(source = "../../img/Monsters/vampire-sheet.png")]		private static const VAMPIRE:Class;
		[Embed(source = "../../snd/Spells/fireblast.mp3")]				private static const FIRE_SND:Class;
		
		// Constants
		protected static const	VAMPIRE_WIDTH:int		= 64;
		protected static const	VAMPIRE_HEIGHT:int		= 64;
		protected static const	NAME:String				= "Vampire";
		protected static const	STARTING_HP:int			= 200;
		protected static const	TRIGGER_RANGE:int		= 4;
		protected static const	FLEE_HP:int				= 0;
		protected static const	HOSTILE_LOG:String		= "A Vampire is hunting you!";
		protected static const	FLEE_LOG:String			= "A Vampire is fleeing from you!";
		
		// States
		protected static const	STATE_WANDERING:int			= 0;
		protected static const	STATE_HOSTILE:int			= 1;
		protected static const	STATE_FLEEING:int			= 2;
		
		// Variables
		protected var	state:int;
		static protected var	fireSfx:Sfx = new Sfx( FIRE_SND );
		
		/**
		 * Create and initialize the Stormtrooper
		 * @param	setRow		Starting position
		 * @param	setColumn	Starting position
		 */
		public function Vampire( setRow:int, setColumn:int )
		{
			monsterName	= NAME;
			HP			= STARTING_HP;
			state		= STATE_WANDERING;
			might		= 50;
			resilience	= 0;
			
			super( VAMPIRE, setRow, setColumn );
		}
		
		/**
		 * Monster's turn, let AI move
		 */
		override public function TakeTurn():void 
		{
			if ( PlayerInRange( TRIGGER_RANGE ) && PlayerVisible() )
			{
				if ( HP >= FLEE_HP )
				{
					if ( state != STATE_HOSTILE )
					{
						state = STATE_HOSTILE;
						GameWorld.log.AddLine( HOSTILE_LOG );
					}
				}
				else
				{
					if ( state != STATE_FLEEING )
					{
						state = STATE_FLEEING;
						GameWorld.log.AddLine( FLEE_LOG );
					}
				}
			}
			else
			{
				state = STATE_WANDERING;
			}
			
			switch( state )
			{
				case STATE_WANDERING:
					Wander();
					break;
				case STATE_HOSTILE:
					if ( PlayerAdjacent() )
						Attack();
					else
						ChasePlayer();
					break;
				case STATE_FLEEING:
					FleePlayer();
			}
			
			super.TakeTurn();
		}
		
		/**
		 * Do attack sound and animation
		 */
		override public function Attack():void 
		{
			fireSfx.play( 0.2 );
			super.Attack();
		}
		
		/**
		 * Drop an item on death
		 */
		override protected function Dead():void
		{
			super.Dead();
			(world as GameWorld).SpawnItem( ObjectFactory.CreateRandomItem(), row, column );
		}
	}
}