package  
{
	import Items.*;
	import Monsters.*;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	
	/**
	 * Class to spawn new items
	 * @author Switchbreak
	 */
	public class ObjectFactory 
	{
		// Assets
		
		static protected const	CONSUMABLES:Array = [Comic, EnergyDrink];
		static protected const	FLOOR_WEAPONS:Array		=
		[[Ribbon, Sword, Blaster],														// Lobby weapons
		[Ribbon, Sword, Blaster, Wand, Saber], 											// Dealer weapons
		[Ribbon, Sword, Blaster, Wand, Saber, Sword2, Blaster2],						// Tabletop
		[Ribbon2, Sword2, Blaster2, Wand2],												// Anime
		[Ribbon2, Sword2, Blaster2, Wand2, Saber2, Sword3],								// Arcade
		[Ribbon3, Sword2, Blaster2, Wand2, Saber2, Sword3, Blaster3, Wand3, Saber3],	// Horror
		[Ribbon3, Sword3, Blaster3, Wand3, Saber3]]										// Hotel
		static protected const	FLOOR_MONSTERS:Array		=
		[[KungFuFighter, Stormtrooper],													// Lobby weapons
		[KungFuFighter, Stormtrooper, Robot, Ninja], 									// Dealer weapons
		[Robot, Ninja, Barbarian, Ultramarine],											// Tabletop
		[Barbarian, Ultramarine, Scout],												// Anime
		[Barbarian, Ultramarine, Knight, Warrior],										// Arcade
		[Knight, Warrior, Sorcerer, Vampire],											// Horror
		[KungFuFighter, Stormtrooper, Robot, Ninja, Barbarian, Ultramarine,
			Scout, Knight, Warrior, Sorcerer, Vampire]]									// Hotel
		
		/**
		 * Spawn a random item
		 * @return	Item object
		 */
		public static function CreateRandomItem():Item
		{
			var itemType:Number = FP.random;
			
			if ( itemType < 0.3 )
			{
				return new Money();
			}
			else if( itemType < 0.8 )
			{
				return new EnergyDrink();
			}
			else if ( itemType < 0.9 )
			{
				return new Comic();
			}
			else
			{
				var weapons:Array = FLOOR_WEAPONS[GameWorld.floorNumber >> 1];
				return new weapons[FP.rand( weapons.length )];
			}
		}
		
		/**
		 * Spawn a random consumable item
		 * @return	Item object
		 */
		public static function CreateRandomStoreItem():Item
		{
			var items:Array = FLOOR_WEAPONS[GameWorld.floorNumber >> 1].concat();
			for each( var consumable:Class in CONSUMABLES )
				items.push( consumable );
			
			return new items[FP.rand( items.length )];
		}
		
		/**
		 * Spawn a random monster
		 * @param	row		Starting position
		 * @param	column	Starting position
		 * @return	Monster object
		 */
		public static function CreateRandomMonster( row:int, column:int ):Monster
		{
			var monsters:Array = FLOOR_MONSTERS[GameWorld.floorNumber >> 1];
			return new monsters[FP.rand( monsters.length )]( row, column );
		}
	}
}