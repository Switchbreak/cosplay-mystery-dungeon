package Objects
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	
	/**
	 * Stairs for descending to the next floor of the convention hall
	 * @author Switchbreak
	 */
	public class Brother extends GameObject 
	{
		// Assets
		[Embed(source = "../../img/NPCs/lilbro.png")]		protected static const	BROTHER:Class;
		
		/**
		 * Create and initialize the stairs
		 * @param	setRow		Position on the map
		 * @param	setColumn	Position on the map
		 */
		public function Brother( setRow:int, setColumn:int )
		{
			var brother:Stamp = new Stamp( BROTHER );
			brother.x = -(brother.width >> 1);
			brother.y = -(brother.height >> 1);
			
			super( setRow, setColumn, true, brother );
		}
		
		/**
		 * Called when player walks over the stairs
		 */
		override public function Activate( target:Object ):void 
		{
			if ( target is Player )
			{
				GameWorld.log.AddLine( "You found your little brother! Congratulations!" );
				(world as GameWorld).FadeOut( false );
			}
		}
	}

}