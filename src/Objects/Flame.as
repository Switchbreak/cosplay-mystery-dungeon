package Objects 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Flame extends GameObject 
	{
		[Embed(source = "../../img/Effects/flame.png")]	protected static const FLAME:Class;
		
		protected var flame:Spritemap;
		
		public function Flame( setRow:int, setColumn:int ) 
		{
			flame = new Spritemap( FLAME, 64, 64 );
			flame.centerOrigin();
			flame.add( "Burn", [1, 0], 10, true );
			
			super( setRow, setColumn, false, flame );
			
			flame.play( "Burn" );
			
			addTween( new Alarm( 1, Finish, Tween.ONESHOT ), true );
			active = true;
		}
		
		public function Finish():void
		{
			world.remove( this );
		}
	}
}