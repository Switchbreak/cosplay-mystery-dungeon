package Objects 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class GasCloud extends GameObject 
	{
		[Embed(source="../../img/Effects/gas.png")]	protected static const GAS_CLOUD:Class;
		
		protected var gascloud:Spritemap;
		
		public function GasCloud( setRow:int, setColumn:int ) 
		{
			gascloud = new Spritemap( GAS_CLOUD, 64, 64 );
			gascloud.centerOrigin();
			gascloud.add( "Puff", [2, 1, 0], 10, true );
			
			super( setRow, setColumn, false, gascloud );
			
			gascloud.play( "Puff" );
			
			addTween( new Alarm( 1, Finish, Tween.ONESHOT ), true );
			active = true;
		}
		
		public function Finish():void
		{
			world.remove( this );
		}
	}
}