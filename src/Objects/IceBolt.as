package Objects 
{
	import flash.geom.Rectangle;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class IceBolt extends GameObject 
	{
		[Embed(source="../../img/Effects/ice.png")]	protected static const ICE:Class;
		
		protected var ice:Image;
		
		public function IceBolt( setRow:int, setColumn:int ) 
		{
			ice = new Image( ICE, new Rectangle( 0, 0, 64, 64 ) );
			ice.originX = ice.width >> 1;
			ice.originY = ice.height;
			
			super( setRow, setColumn, false, ice );
			
			addTween( new Alarm( 0.4, Finish, Tween.ONESHOT ), true );
			active = true;
		}
		
		public function Finish():void
		{
			world.remove( this );
		}
	}
}