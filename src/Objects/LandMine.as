package Objects 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class LandMine extends GameObject 
	{
		[Embed(source = "../../img/Effects/landmine.png")]	protected static const LAND_MINE:Class;
		[Embed(source = "../../snd/Spells/landmine.mp3")]	protected static const EXPLOSION:Class;
		
		protected var landmine:Spritemap;
		
		protected static var explosionSfx:Sfx = new Sfx( EXPLOSION );
		
		public function LandMine( setRow:int, setColumn:int ) 
		{
			landmine = new Spritemap( LAND_MINE, 64, 64 );
			landmine.centerOrigin();
			landmine.add( "Beep", [0, 1], 10, true );
			
			super( setRow, setColumn, false, landmine );
			
			landmine.play( "Beep" );
		}
		
		override public function Activate( target:Object ):void 
		{
			explosionSfx.play( 0.2 );
			
			if ( target is Monster )
				(target as Monster).Hurt( 50 + FP.rand( 10 ) );
			else if ( target is Player )
				(target as Player).Hurt( 50 + FP.rand( 10 ) );
			
			world.remove( this );
		}
	}
}