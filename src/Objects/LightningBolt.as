package Objects 
{
	import flash.geom.Rectangle;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class LightningBolt extends GameObject 
	{
		[Embed(source="../../img/Effects/lightningbolt.png")]	protected static const LIGHTNING:Class;
		
		protected var lightning:Image;
		
		public function LightningBolt( setRow:int, setColumn:int, vertical:Boolean ) 
		{
			lightning = new Image( LIGHTNING, new Rectangle( 0, 0, 64, 64 ) );
			lightning.centerOrigin();
			
			if ( vertical )
				lightning.angle = 90;
			
			super( setRow, setColumn, false, lightning );
			
			addTween( new Alarm( 0.4, Finish, Tween.ONESHOT ), true );
			active = true;
		}
		
		public function Finish():void
		{
			world.remove( this );
		}
	}
}