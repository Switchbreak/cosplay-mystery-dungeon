package Objects
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import Sound.RandomSound;
	
	/**
	 * Stairs for descending to the next floor of the convention hall
	 * @author Switchbreak
	 */
	public class Stairs extends GameObject 
	{
		// Assets
		[Embed(source = "../../img/stairs.png")]		protected static const	STAIRS:Class;
		[Embed(source = "../../snd/Player/stairs.mp3")]	protected static const	FOOTSTEP_STAIRS_SND:Class;
		
		// Variables
		protected var footstepStairSound:RandomSound = new RandomSound(
		    [FOOTSTEP_STAIRS_SND],
		    0.8);
		
		/**
		 * Create and initialize the stairs
		 * @param	setRow		Position on the map
		 * @param	setColumn	Position on the map
		 */
		public function Stairs( setRow:int, setColumn:int )
		{
			var stairs:Stamp = new Stamp( STAIRS );
			stairs.x = -(stairs.width >> 1);
			stairs.y = -(stairs.height >> 1);
			
			super( setRow, setColumn, false, stairs );
		}
		
		/**
		 * Called when player walks over the stairs
		 */
		override public function Activate( target:Object ):void 
		{
			if ( target is Player )
			{
				(world as GameWorld).Descend();
				footstepStairSound.play();
			}
		}
	}

}