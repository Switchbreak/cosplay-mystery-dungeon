package Objects 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import UI.BuyPanel;
	import Types.Item;
	
	/**
	 * Vendor NPC
	 * @author Switchbreak
	 */
	public class Vendor extends GameObject 
	{
		// Assets
		[Embed(source = "../../img/NPCs/catvendor.png")]	protected static const VENDOR:Class;
		[Embed(source = "../../img/Shadow.png")]			protected static const	SHADOW:Class;
		
		// Constants
		protected static const	VENDOR_GREETING:String	= "Buy something, nyan!";
		protected static const	ITEM_COUNT:int			= 3;
		
		// Variables
		protected var items:Vector.<Item>;
		
		/**
		 * Create and initialize the stairs
		 * @param	setRow		Position on the map
		 * @param	setColumn	Position on the map
		 */
		public function Vendor( setRow:int, setColumn:int )
		{
			var vendor:Stamp = new Stamp( VENDOR );
			vendor.x = -(vendor.width >> 1);
			vendor.y = -vendor.height;
			
			var shadow:Stamp = new Stamp( SHADOW );
			shadow.x = -(shadow.width >> 1);
			shadow.y = -(shadow.height >> 1);
			
			super( setRow, setColumn, true, new Graphiclist( shadow, vendor ) );
			
			CreateItems();
		}
		
		/**
		 * Called when player walks into the vendor
		 */
		override public function Activate( target:Object ):void 
		{
			if ( target is Player )
			{
				GameWorld.log.AddLine( VENDOR_GREETING );
				(world as GameWorld).WaitOnAction();
				world.add( new BuyPanel( items ) );
			}
		}
		
		/**
		 * Create the items for sale when vendor is first spawned
		 */
		protected function CreateItems():void 
		{
			items = new Vector.<Item>( ITEM_COUNT )
			for (var i:int = 0; i < ITEM_COUNT; ++i) 
			{
				items[i] = ObjectFactory.CreateRandomStoreItem();
			}
		}
	}
}