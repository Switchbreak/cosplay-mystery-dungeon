package  
{
	import Items.Comic;
	import mx.utils.StringUtil;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import Types.Item;
	import Types.MapPosition;
	import Sound.RandomSound;
	import UI.Floater;
	
	/**
	 * Game object for player
	 * @author Switchbreak
	 */
	public class Player extends Entity 
	{
	    // Embeds
		[Embed(source = "../img/Shadow.png")]						private static const SHADOW:Class;
		[Embed(source = "../img/Player/fencer-female-sheet2.png")]	private static const PLAYER_IMAGE:Class;
		
	    [Embed(source = "../snd/Player/footstep_indoor.mp3")]  private static const FOOTSTEP_INDOOR_SND:Class;
		[Embed(source = "../snd/Player/footstep_indoor2.mp3")] private static const FOOTSTEP_INDOOR_SND2:Class;
		[Embed(source = "../snd/Player/footstep_indoor3.mp3")] private static const FOOTSTEP_INDOOR_SND3:Class;
		
		[Embed(source = "../snd/Player/collectitem.mp3")] private static const COLLECT_ITEM_SND:Class;
		
		[Embed(source = "../snd/Weapons/fists1.mp3")] 	private static const FISTS_SND1:Class;
		[Embed(source = "../snd/Weapons/fists2.mp3")] 	private static const FISTS_SND2:Class;
		[Embed(source = "../snd/Weapons/fists3.mp3")] 	private static const FISTS_SND3:Class;
		[Embed(source = "../snd/Weapons/fists4.mp3")] 	private static const FISTS_SND4:Class;
		
		/*[Embed(source = "../snd/Player/hit_male1.mp3")]    private static const HIT_MALE_SND1:Class;
		[Embed(source = "../snd/Player/hit_male2.mp3")]    private static const HIT_MALE_SND2:Class;
		[Embed(source = "../snd/Player/hit_male3.mp3")]    private static const HIT_MALE_SND3:Class;
		[Embed(source = "../snd/Player/hit_male4.mp3")]    private static const HIT_MALE_SND4:Class;
		*/
		
		// Constants
		protected static const	KEY_UP:String			= "Up";
		protected static const	KEY_DOWN:String			= "Down";
		protected static const	KEY_LEFT:String			= "Left";
		protected static const	KEY_RIGHT:String		= "Right";
		protected static const	KEY_TURBO:String		= "Turbo";
		protected static const	WALK_RATE:Number		= 0.4;
		protected static const	ATTACK_RATE:Number		= 0.3;
		protected static const	PLAYER_WIDTH:int		= 64;
		protected static const	PLAYER_HEIGHT:int		= 64;
		protected static const	PICK_UP_LOG:String		= "Picked up a {0}.";
		protected static const	USE_LOG:String			= "Used a {0}.";
		protected static const	THROW_LOG:String		= "Threw away a {0}.";
		protected static const	EQUIP_LOG:String		= "Equipped a {0}.";
		protected static const	UNEQUIP_LOG:String		= "Unequipped a {0}.";
		protected static const	WEAR_LOG:String			= "Put on a {0}.";
		protected static const	REMOVE_LOG:String		= "Took off a {0}.";
		protected static const	SELL_LOG:String			= "Sold a {0} for ${1}.";
		protected static const	ATTACK_LOG:String		= "Attacked a {0} with {1}.";
		protected static const	HURT_LOG:String			= "You have been hit for -{0} HP.";
		protected static const	HEAL_LOG:String			= "You have been healed for +{0} HP.";
		protected static const	DIE_LOG:String			= "You have died.";
		protected static const	WEAPON_FISTS:String		= "Fists";
		public static const		MAX_HP:int				= 200;
		public static const		STARTING_MIGHT:int		= 10;
		public static const		STARTING_RESILIENCE:int	= 5;
		public static const		STARTING_EVASION:int	= 15;
		public static const		STAT_RANGE:int			= 2;
		
		// Directions
		public static const	FACING_DOWN:int		= 0;
		public static const	FACING_LEFT:int		= 1;
		public static const	FACING_RIGHT:int	= 2;
		public static const	FACING_UP:int		= 3;
		
		// Animations
		protected static const	ANIM_IDLE_NAMES:Array		= ["Idle Down", "Idle Left", "Idle Right", "Idle Up"]
		protected static const	ANIM_IDLE_FRAMES:Array		= [[0], [4], [4], [8]];
		protected static const	ANIM_WALK_NAMES:Array		= ["Walk Down", "Walk Left", "Walk Right", "Walk Up"]
		protected static const	ANIM_WALK_FRAMES:Array		= [[0, 1, 2, 3], [4, 5, 6, 7], [4, 5, 6, 7], [8, 9, 10, 11]];
		protected static const	ANIM_ATTACK_NAMES:Array		= ["Attack Down", "Attack Left", "Attack Right", "Attack Up"]
		protected static const	ANIM_ATTACK_FRAMES:Array	= [[0, 12, 0], [4, 13, 4], [4, 13, 4], [8, 15, 8]];
		protected static const	ANIM_HURT_NAMES:Array		= ["Hurt Down", "Hurt Left", "Hurt Right", "Hurt Up"]
		protected static const	ANIM_HURT_FRAMES:Array		= [[0, 16, 0], [4, 17, 4], [4, 17, 4], [8, 19, 8]];
		protected static const	ANIM_DIE_NAMES:Array		= ["Die Down", "Die Left", "Die Right", "Die Up"]
		protected static const	ANIM_DIE_FRAMES:Array		= [[16, 17, 19, 20], [17, 19, 20], [17, 16, 18, 19, 20], [19, 18, 16, 17, 19, 20]];
		protected static const	ANIM_RATE:Number			= 10;
		
		// Variables
		protected var	playerSprite:Spritemap;
		protected var	motionTween:LinearMotion;
		public var		confused:Boolean;
		public var		confusedTimer:int;
		public var		row:int;
		public var		column:int;
		public var		facing:int;
		public var		inventory:Vector.<Item>;
		public var		wearing:Vector.<Item>;
		public var		equipped:Item;
		public var		dead:Boolean;
		public var		playersTurn:Boolean;
		public var		turboSpeed:Boolean;
		
		public var		HP:int;
		public var		might:int;
		public var		resilience:int;
		public var		evasion:int;
		public var		money:Number;
		
		// Sounds
		protected var footstepIndoorSounds:Array = [
			new Sfx( FOOTSTEP_INDOOR_SND ),
			new Sfx( FOOTSTEP_INDOOR_SND2 ),
			new Sfx( FOOTSTEP_INDOOR_SND ),
			new Sfx( FOOTSTEP_INDOOR_SND3 )];
		protected var footstepSoundIndex:int = 0;
		protected static const FOOTSTEP_INDOOR_VOLUME:Number = 0.1;
		
		protected var collectItemSound:RandomSound = new RandomSound(
		    [COLLECT_ITEM_SND],
		    0.3);
		protected var fistsSound:RandomSound = new RandomSound(
		    [FISTS_SND1, FISTS_SND2, FISTS_SND3, FISTS_SND4],
		    0.8);
		/*protected var hitMaleSound:RandomSound = new RandomSound(
		    [HIT_MALE_SND1, HIT_MALE_SND2, HIT_MALE_SND3, HIT_MALE_SND4],
		    0.4);*/
		        
		/**
		 * Create and initialize the player
		 */
		public function Player() 
		{
			Input.define( KEY_UP, Key.UP, Key.W, Key.Y, Key.K, Key.U, Key.NUMPAD_7, Key.NUMPAD_8, Key.NUMPAD_9 );
			Input.define( KEY_DOWN, Key.DOWN, Key.S, Key.B, Key.J, Key.N, Key.NUMPAD_1, Key.NUMPAD_2, Key.NUMPAD_3 );
			Input.define( KEY_LEFT, Key.LEFT, Key.A, Key.B, Key.H, Key.Y, Key.NUMPAD_1, Key.NUMPAD_4, Key.NUMPAD_7 );
			Input.define( KEY_RIGHT, Key.RIGHT, Key.D, Key.N, Key.L, Key.U, Key.NUMPAD_3, Key.NUMPAD_6, Key.NUMPAD_9 );
			Input.define( KEY_TURBO, Key.SHIFT );
			
			playerSprite = new Spritemap( PLAYER_IMAGE, PLAYER_WIDTH, PLAYER_HEIGHT );
			playerSprite.originX = playerSprite.width >> 1;
			playerSprite.originY = playerSprite.height;
			
			for (var i:int = 0; i < 4; i++) 
			{
				playerSprite.add( ANIM_IDLE_NAMES[i], ANIM_IDLE_FRAMES[i], ANIM_RATE, false );
				playerSprite.add( ANIM_WALK_NAMES[i], ANIM_WALK_FRAMES[i], ANIM_RATE, true );
				playerSprite.add( ANIM_ATTACK_NAMES[i], ANIM_ATTACK_FRAMES[i], ANIM_RATE, false );
				playerSprite.add( ANIM_HURT_NAMES[i], ANIM_HURT_FRAMES[i], ANIM_RATE, false );
				playerSprite.add( ANIM_DIE_NAMES[i], ANIM_DIE_FRAMES[i], ANIM_RATE, false );
			}
			
			var shadow:Stamp = new Stamp( SHADOW );
			shadow.x = -(shadow.width >> 1);
			shadow.y = -(shadow.height >> 1);
			
			super( 0, 0, new Graphiclist( shadow, playerSprite ) );
			
			motionTween = new LinearMotion( Arrive, Tween.PERSIST );
			addTween( motionTween );
			
			HP					= MAX_HP;
			might				= STARTING_MIGHT - STAT_RANGE + FP.rand( STAT_RANGE << 1);
			resilience			= STARTING_RESILIENCE - STAT_RANGE + FP.rand( STAT_RANGE << 1);
			evasion				= STARTING_EVASION - STAT_RANGE + FP.rand( STAT_RANGE << 1);
			money				= 0;
			
			turboSpeed			= false;
			dead				= false;
			layer				= -1 - row;
			inventory			= new Vector.<Item>();
			wearing				= new Vector.<Item>();
		}
		
		/**
		 * On being added to a new gameworld
		 */
		override public function added():void 
		{
			super.added();
			
			motionTween.active	= false;
			playersTurn			= true;
			facing				= FACING_DOWN;
			
			playerSprite.play( ANIM_IDLE_NAMES[facing], true );
			
			FindAvailableSpace();
			world.camera.x = x - FP.halfWidth;
			world.camera.y = y - FP.halfHeight;
			GameWorld.currentFloor.UpdateTileSight( row, column );
		}
		
		/**
		 * Handle input and motion
		 */
		override public function update():void 
		{
			if ( motionTween.active )
				FollowTween();
			
			if ( playersTurn )
			{
				var destination:MapPosition = HandleInput();
				MoveTo( destination.row, destination.column );
			}
			
			super.update();
		}
		
		/**
		 * Place the player at the first empty spot on the map you can find
		 */
		public function FindAvailableSpace():void
		{
			var tries:int = 1000;
			
			while ( tries > 0 )
			{
				row		= FP.rand( FloorMap.MAP_HEIGHT );
				column	= FP.rand( FloorMap.MAP_WIDTH );
				
				if ( GameWorld.currentFloor.GetTileAtLocation( row, column ) == FloorMap.GAME_TILE_FLOOR )
				{
					x = column * FloorMap.TILE_WIDTH + (FloorMap.TILE_WIDTH >> 1);
					y = row * FloorMap.TILE_HEIGHT + (FloorMap.TILE_HEIGHT >> 1);
					
					return;
				}
				
				--tries;
			}
			
			throw new Error( "Could not find an available space on the map to place the player" );
		}
		
		/**
		 * Pick up an item and add it to the inventory
		 * @param	item	Item to pick up
		 */
		public function PickUp( item:Item ):void
		{
			inventory.push( item );
			GameWorld.inventory.ScrollToNewest();
			
			GameWorld.log.AddLine( StringUtil.substitute( PICK_UP_LOG, item.name ) );
			
			collectItemSound.play();
			
			item.PickUp( this );
		}
		
		/**
		 * Use an item - trigger its effect then destroy it
		 * @param	item	Item to use
		 */
		public function UseItem( item:Item ):void
		{
			if ( item.usable )
			{
				DropItem( item );
				GameWorld.items.splice( GameWorld.items.indexOf( item ), 1 );
				GameWorld.log.AddLine( StringUtil.substitute( USE_LOG, item.name ) );
				item.Use( this, null );
				(world as GameWorld).AdvanceTurn();
			}
		}
		
		/**
		 * Throw an item
		 * @param	item	Item to throw
		 */
		public function ThrowItem( item:Item ):void
		{
			if ( item.throwable )
			{
				DropItem( item );
				world.add( new WorldItem( row, column, item ) );
				// TODO: throw item physics
				GameWorld.log.AddLine( StringUtil.substitute( THROW_LOG, item.name ) );
				item.Throw();
				(world as GameWorld).AdvanceTurn();
			}
		}
		
		/**
		 * Equip an item
		 * @param	item	Item to equip
		 */
		public function EquipItem( item:Item ):void
		{
			if ( item.equipable )
			{
				if ( equipped != null )
					RemoveItem( equipped );
				equipped = item;
				item.equipped = true;
				GameWorld.log.AddLine( StringUtil.substitute( EQUIP_LOG, item.name ) );
				
				// FIXME: add unique per-item equip sounds
				collectItemSound.play();
				
				item.Equip( this );
				(world as GameWorld).AdvanceTurn();
			}
		}
		
		/**
		 * Wear an item
		 * @param	item	Item to wear
		 */
		public function WearItem( item:Item ):void
		{
			if ( item.wearable )
			{
				item.wearing = true;
				wearing.push( item );
				GameWorld.log.AddLine( StringUtil.substitute( WEAR_LOG, item.name ) );
				
				// FIXME: add unique per-item wear sounds
				collectItemSound.play();
				
				item.Wear( this );
				(world as GameWorld).AdvanceTurn();
			}
		}
		
		/**
		 * Remove a worn or equipped item
		 * @param	item	Item to remove
		 */
		public function RemoveItem( item:Item ):void
		{
			if ( item.wearing )
			{
				item.wearing = false;
				wearing.splice( wearing.indexOf( item ), 1 );
				GameWorld.log.AddLine( StringUtil.substitute( REMOVE_LOG, item.name ) );
				
				collectItemSound.play();
				
				item.Remove( this );
				(world as GameWorld).AdvanceTurn();
			}
			if ( item.equipped )
			{
				item.equipped = false;
				equipped = null;
				GameWorld.log.AddLine( StringUtil.substitute( UNEQUIP_LOG, item.name ) );
				
				collectItemSound.play();
				
				item.Remove( this );
				(world as GameWorld).AdvanceTurn();
			}
		}
		
		/**
		 * Sell an item
		 * @param	item	Item to sell
		 */
		public function SellItem( item:Item ):void
		{
			DropItem( item );
			GameWorld.items.splice( GameWorld.items.indexOf( item ), 1 );
			GameWorld.log.AddLine( StringUtil.substitute( SELL_LOG, item.name, item.value ) );
			money += item.value;
			
			(world as GameWorld).UpdateMoneyCounter();
			GameWorld.inventory.UpdateInventory();
		}
		
		/**
		 * Attack a monster with the equipped weapon
		 * @param	monster		Monster to attack
		 */
		public function Attack( monster:Monster ):void
		{
			var weaponName:String;
			var attackValue:int = might;
			if ( equipped == null )
			{
				weaponName = WEAPON_FISTS;
				fistsSound.play();
			}
			else
			{
				weaponName = equipped.name;
				attackValue += equipped.attack;
				equipped.Use( this, monster );
			}
			
			GameWorld.log.AddLine( StringUtil.substitute( ATTACK_LOG, monster.monsterName, weaponName ) );
			monster.Hurt( attackValue - 5 + FP.rand( 10 ) );
			
			playerSprite.play( ANIM_ATTACK_NAMES[facing], true );
			addTween( new Alarm( ATTACK_RATE, (world as GameWorld).AdvanceTurn, Tween.ONESHOT ), true );
		}
		
		/**
		 * Drop an item from the inventory
		 * @param	item	Item to drop
		 */
		public function DropItem( item:Item ):void
		{
			if ( item.wearing || item.equipped )
				RemoveItem( item );
			inventory.splice( inventory.indexOf( item ), 1 );
		}
		
		/**
		 * Take damage
		 * @param	damage	Amount of damage to take
		 */
		public function Hurt( damage:int ):void
		{
			var defense:int = resilience;
			for each( var item:Item in wearing )
			{
				defense += item.defense;
			}
			
			damage -= defense;
			if ( damage < 0 )
				damage = 0;
			
			GameWorld.log.AddLine( StringUtil.substitute( HURT_LOG, damage ) );
			playerSprite.play( ANIM_HURT_NAMES[facing], true );
			
			world.add( new Floater( "-" + damage.toString(), x, y - PLAYER_HEIGHT, 0xFFFF00 ) );
			
			HP -= damage;
			if ( HP <= 0 )
			{
				HP = 0;
				Die();
			}
		}
		
		/**
		 * Player has been healed
		 * @param	healAmount	Amount of healing
		 */
		public function Heal( healAmount:int ):void
		{
			GameWorld.log.AddLine( StringUtil.substitute( HEAL_LOG, healAmount ) );
			
			world.add( new Floater( "+" + healAmount.toString(), x, y - PLAYER_HEIGHT, 0x00FF00 ) );
			
			HP += healAmount;
			if ( HP > MAX_HP )
				HP = MAX_HP;
		}
		
		/**
		 * Die when health reaches 0
		 */
		public function Die():void
		{
			GameWorld.log.AddLine( DIE_LOG );
			dead = true;
			motionTween.active = false;
			playerSprite.play( ANIM_DIE_NAMES[facing], true );
			addTween( new Alarm( 1, (world as GameWorld).FadeOut, Tween.ONESHOT ), true );
		}
		
		public function Confuse( duration:int ):void
		{
			confused = true;
			confusedTimer = duration;
		}
		
		/**
		 * Handle keyboard input for player
		 */
		protected function HandleInput():MapPosition 
		{
			var position:MapPosition = new MapPosition( row, column );
			
			if ( !dead )
			{
				turboSpeed = Input.check( KEY_TURBO );
				if ( Input.check( KEY_UP ) )
					--position.row;
				if ( Input.check( KEY_DOWN ) )
					++position.row;
				if ( Input.check( KEY_LEFT ) )
					--position.column;
				if ( Input.check( KEY_RIGHT ) )
					++position.column;
				
				if ( confused )
				{
					position.row	= row + (row - position.row);
					position.column	= column + (column - position.column);
				}
			}
			
			return position;
		}
		
		/**
		 * Move to a destination and check for collisions
		 * @param	destRow		Destination map coordinates
		 * @param	destColumn	Destination map coordinates
		 */
		protected function MoveTo( destRow:int, destColumn:int ):void 
		{
			if ( row == destRow && column == destColumn )
				return;
			
			SetFacing( destRow, destColumn );
			
			if ( destRow < 0 || destRow >= FloorMap.MAP_HEIGHT )
				return;
			if ( destColumn < 0 || destColumn >= FloorMap.MAP_WIDTH )
				return;
			
			if ( GameWorld.currentFloor.GetTileAtLocation( destRow, destColumn ) != FloorMap.GAME_TILE_FLOOR )
				return;
			
			playersTurn = false;
			
			if ( ObjectCollisionCheck( destRow, destColumn ) )
			{
				(world as GameWorld).AdvanceTurn();
				return;
			}
			
			var monster:Monster = MonsterCollisionCheck( destRow, destColumn )
			if ( monster != null )
			{
				Attack( monster );
				return;
			}
			
			WalkTo( destRow, destColumn );
			PlayFootstepSound();
		}
		
		/**
		 * Set the facing toward a direction
		 * @param	destRow		
		 * @param	destColumn
		 */
		protected function SetFacing( destRow:int, destColumn:int ):void 
		{
			playerSprite.flipped = false;
			
			if ( destRow < row )
				facing = FACING_UP;
			else if ( destRow > row )
				facing = FACING_DOWN;
			else if ( destColumn < column )
				facing = FACING_LEFT;
			else
			{
				facing = FACING_RIGHT;
				playerSprite.flipped = true;
			}
			
			playerSprite.play( ANIM_IDLE_NAMES[facing], true );
		}
		
		/**
		 * Move player to destination row
		 * @param	destRow
		 * @param	destColumn
		 */
		protected function WalkTo( destRow:int, destColumn:int ):void
		{
			row		= destRow;
			column	= destColumn;
			layer	= -1 - row;
			
			playerSprite.play( ANIM_WALK_NAMES[facing], true );
			motionTween.setMotion(	x, y,
									column * FloorMap.TILE_WIDTH + (FloorMap.TILE_WIDTH >> 1),
									row * FloorMap.TILE_HEIGHT + (FloorMap.TILE_HEIGHT >> 1),
									(turboSpeed ? (WALK_RATE / 2) : WALK_RATE), null );
			
			(world as GameWorld).WaitOnAction();
			(world as GameWorld).AdvanceTurn();
		}
		
		/**
		 * Called by motion tween when it arrives at its goal
		 */
		protected function Arrive():void
		{
			FollowTween();
			playerSprite.play( ANIM_IDLE_NAMES[facing], true );
			
			var worldItem:WorldItem = ItemCollisionCheck();
			if ( worldItem != null )
			{
				PickUp( worldItem.item );
				world.remove( worldItem );
			}
			
			GameWorld.currentFloor.UpdateTileSight( row, column );
			(world as GameWorld).EndTurn();
		}
		
		/**
		 * Move to the current position of the motion tweener
		 */
		protected function FollowTween():void
		{
			x = motionTween.x;
			y = motionTween.y;
		}
		
		/**
		 * Play footstep sound, cycling through a list of variants
		 */
		protected function PlayFootstepSound():void
		{
			footstepSoundIndex = (footstepSoundIndex + 1) % footstepIndoorSounds.length;
			footstepIndoorSounds[footstepSoundIndex].play(FOOTSTEP_INDOOR_VOLUME);
		}
		
		/**
		 * Check if the player is colliding with an item
		 * @return	Reference to item found, or null if no collision
		 */
		protected function ItemCollisionCheck():WorldItem
		{
			var items:Vector.<WorldItem> = new Vector.<WorldItem>();
			world.getClass( WorldItem, items );
			
			for each( var item:WorldItem in items )
			{
				if ( item.row == row && item.column == column )
					return item;
			}
			
			return null;
		}
		
		/**
		 * Check if the player is colliding with an item
		 * @param	destRow		Destination map coordinates
		 * @param	destColumn	Destination map coordinates
		 * @return	Reference to item found, or null if no collision
		 */
		protected function MonsterCollisionCheck( destRow:int, destColumn:int ):Monster
		{
			var monsters:Vector.<Monster> = new Vector.<Monster>();
			world.getClass( Monster, monsters );
			
			for each( var monster:Monster in monsters )
			{
				if ( monster.row == destRow && monster.column == destColumn )
					return monster;
			}
			
			return null;
		}
		
		/**
		 * Check if the player is colliding with an item
		 * @param	destRow		Destination map coordinates
		 * @param	destColumn	Destination map coordinates
		 * @return	Reference to item found, or null if no collision
		 */
		protected function ObjectCollisionCheck( destRow:int, destColumn:int ):Boolean
		{
			var objects:Vector.<GameObject> = new Vector.<GameObject>();
			world.getClass( GameObject, objects );
			
			for each( var object:GameObject in objects )
			{
				if ( object.row == destRow && object.column == destColumn )
				{
					object.Activate( this );
					return object.blocking;
				}
			}
			
			return false;
		}
	}
}