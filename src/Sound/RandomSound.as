package Sound {
    import net.flashpunk.Sfx;
    
    public class RandomSound {
        protected var sounds:Array = new Array();
        protected var lastSoundPlayed:int;
    
        protected var _volume:Number;
    
        public function RandomSound(embeds:Array, volume:Number) {
            for(var i:int=0; i < embeds.length; i++) {
                sounds.push(new Sfx(embeds[i]));
            }
            lastSoundPlayed = 0;
            _volume = volume;
        }

        public function play():void {
            var soundNum:int;
            
            if(sounds.length > 1) {
                // choose any sound that wasn't the sound played most recently
            
                soundNum = Math.floor(Math.random() * (sounds.length - 1));
                if ( soundNum >= lastSoundPlayed ) {
                    soundNum += 1;
                }
            } else {
                soundNum = 0;
            } 
            sounds[soundNum].play(_volume);
            lastSoundPlayed = soundNum;
        }
    }
}
