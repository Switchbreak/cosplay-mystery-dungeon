package Types 
{
	import flash.geom.Rectangle;
	
	/**
	 * Node in a BSP tree used for map generation
	 * @author Switchbreak
	 */
	public class BSPNode 
	{
		public var leftChild:BSPNode;
		public var rightChild:BSPNode;
		
		public var leaf:Rectangle;
		public var vertical:Boolean;
		public var position:int;
	}

}