package Types 
{
	import net.flashpunk.Graphic;
	
	/**
	 * Describes a collectible game item
	 * @author Switchbreak
	 */
	public class Item 
	{
		public static const	EQUIPABLE_NOT:int			= 0;
		public static const	EQUIPABLE_SINGLE_HANDED:int	= 1;
		public static const	EQUIPABLE_DOUBLE_HANDED:int	= 2;
		
		public var name:String;
		public var description:String;
		public var graphic:Graphic;
		public var usable:Boolean;
		public var throwable:Boolean;
		public var equipable:int;
		public var wearable:Boolean;
		public var equipped:Boolean;
		public var wearing:Boolean;
		public var active:Boolean;
		public var attack:int;
		public var defense:int;
		public var width:int;
		public var height:int;
		public var value:Number;
		
		/**
		 * Called when item is picked up, override to add behavior
		 */
		public function PickUp( target:Player ):void { }
		
		/**
		 * Called when item is used, override to add behavior
		 */
		public function Use( subject:Player, object:Monster ):void { }
		
		/**
		 * Called when item is thrown, override to add behavior
		 */
		public function Throw():void { }
		
		/**
		 * Called when item is equipped, override to add behavior
		 */
		public function Equip( target:Player ):void { }
		
		/**
		 * Called when item is worn, override to add behavior
		 */
		public function Wear( target:Player ):void { }
		
		/**
		 * Called when equipped/worn item is removed, override to add behavior
		 */
		public function Remove( target:Player ):void { }
		
		/**
		 * Called every turn if active flag is set, override to add behavior
		 */
		public function Update():void { }
	}

}