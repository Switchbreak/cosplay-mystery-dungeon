package Types 
{
	/**
	 * Represents a position on a tile map
	 * @author Switchbreak
	 */
	public class MapPosition 
	{
		public var row:int;
		public var column:int;
		
		/**
		 * Create and optionally initialize the position
		 * @param	setRow		Map coordinates
		 * @param	setColumn	Map coordinates
		 */
		public function MapPosition( setRow:int = 0, setColumn:int = 0 )
		{
			row = setRow;
			column = setColumn;
		}
	}

}