package UI 
{
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	
	/**
	 * Left and right navigation button for inventory
	 * @author Switchbreak
	 */
	public class Button extends Spritemap 
	{
		// Constants
		static private const UNPRESSED_FRAME:int	= 0;
		static private const PRESSED_FRAME:int		= 1;
		
		// Variables
		protected var buttonEvent:Function;
		
		/**
		 * Create and initialize the nav button
		 * @param	setX			Starting position of the button
		 * @param	setY			Starting position of the button
		 * @param	setFlipped		True if facing right, false if facing left
		 * @param	setButtonEvent	Function to call when button is pressed
		 */
		public function Button( source:*, frameWidth:int, frameHeight:int, setX:int, setY:int, setFlipped:Boolean, setButtonEvent:Function )
		{
			super( source, frameWidth, frameHeight );
			
			active		= true;
			relative	= false;
			scrollX		= 0;
			scrollY		= 0;
			x			= setX;
			y			= setY;
			flipped		= setFlipped;
			buttonEvent	= setButtonEvent;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( frame == UNPRESSED_FRAME )
			{
				if ( Input.mousePressed && MouseOver() )
					frame = PRESSED_FRAME;
			}
			else
			{
				if ( MouseOver() )
				{
					if ( Input.mouseReleased )
					{
						frame = UNPRESSED_FRAME;
						buttonEvent();
					}
				}
				else
				{
					frame = UNPRESSED_FRAME;
				}
			}
			
			super.update();
		}
		
		/**
		 * Checks if the mouse is over the nav button
		 * @return	True if mouse is over, false otherwise
		 */
		protected function MouseOver():Boolean
		{
			return (Input.mouseX >= x && Input.mouseX <= x + width && Input.mouseY >= y && Input.mouseY <= y + height);
		}
	}
}