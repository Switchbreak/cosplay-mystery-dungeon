package UI 
{
	import Items.Comic;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import Types.Item;
	
	/**
	 * UI Pane to show Buy Screen
	 * @author Switchbreak
	 */
	public class BuyPanel extends Entity 
	{
		// Assets
		[Embed(source = "../../img/Inventory/CloseButton.png")]	protected static const CLOSE_BUTTON:Class;
		
		// Constants
		protected static const	PANEL_WIDTH:int			= 300;
		protected static const	PANEL_HEIGHT:int		= 200;
		protected static const	CLOSE_WIDTH:int			= 32;
		protected static const	CLOSE_HEIGHT:int		= 32;
		protected static const	CLOSE_X:int				= 258;
		protected static const	CLOSE_Y:int				= 10;
		protected static const	TITLE_TEXT:String		= "Buy something, nyan!"
		protected static const	TITLE_X:int				= 10;
		protected static const	TITLE_Y:int				= 10;
		protected static const	INSTRUCTION_TEXT:String	= "Bring up the inventory to sell your items"
		protected static const	INSTRUCTION_X:int		= 10;
		protected static const	INSTRUCTION_Y:int		= 165;
		protected static const	ITEM_WINDOW_WIDTH:int	= 100;
		protected static const	BUY_LIST_X:int			= 25;
		protected static const	BUY_LIST_Y:int			= 45;
		protected static const	BUY_COUNT:int			= 3;
		protected static const	PRICE_Y:int				= 135;
		protected static const	PRICE_WIDTH:int			= 64;
		protected static const	NOT_ENOUGH_MONEY:String	= "You don't have enough money, nyan!"
		
		// Variables
		protected var vendorItems:Vector.<Item>;
		
		/**
		 * Create and initialize the buy panel
		 * @param	items	List of items for sale
		 */
		public function BuyPanel( items:Vector.<Item> ) 
		{
			super( FP.halfWidth - (PANEL_WIDTH >> 1), FP.halfHeight - (PANEL_HEIGHT >> 1), new Graphiclist() );
			
			graphic.scrollX	= 0;
			graphic.scrollY	= 0;
			layer			= GameWorld.HUD_LAYER + 1;
			vendorItems		= items;
			
			GameWorld.inventory.UpdateInventory();
			
			var panelBackground:Image = Image.createRect( PANEL_WIDTH, PANEL_HEIGHT, 0, 0.5 );
			addGraphic( panelBackground );
			
			var titleText:Text = new Text( TITLE_TEXT, TITLE_X, TITLE_Y, { size: 20 } );
			addGraphic( titleText );
			
			var instructionText:Text = new Text( INSTRUCTION_TEXT, INSTRUCTION_X, INSTRUCTION_Y );
			addGraphic( instructionText );
			
			var closeButton:Button = new Button( CLOSE_BUTTON, CLOSE_WIDTH, CLOSE_HEIGHT, x + CLOSE_X, y + CLOSE_Y, false, ClosePanel );
			addGraphic( closeButton );
			
			var itemWindowX:int = BUY_LIST_X;
			for (var i:int = 0; i < BUY_COUNT; i++) 
			{
				var itemWindow:ItemWindow = new ItemWindow( x + itemWindowX, x + BUY_LIST_Y, vendorItems[i], BuyItem );
				addGraphic( itemWindow );
				
				if ( vendorItems[i] != null )
				{
					var itemPrice:Text = new Text( "$" + vendorItems[i].value.toFixed( 2 ), itemWindowX, PRICE_Y, { align: "center", width: PRICE_WIDTH } );
					addGraphic( itemPrice );
				}
				
				itemWindowX += ITEM_WINDOW_WIDTH;
			}
		}
		
		/**
		 * Close the panel
		 */
		protected function ClosePanel():void
		{
			(world as GameWorld).EndTurn();
			world.remove( this );
		}
		
		/**
		 * Buy a selected item
		 * @param	item	Item to buy
		 */
		protected function BuyItem( item:Item ):void
		{
			if ( GameWorld.player.money >= item.value )
			{
				GameWorld.player.money -= item.value;
				vendorItems[vendorItems.indexOf( item )] = null;
				GameWorld.items.push( item );
				GameWorld.player.PickUp( item );
				ClosePanel();
			}
			else
			{
				GameWorld.log.AddLine( NOT_ENOUGH_MONEY );
			}
		}
	}
}