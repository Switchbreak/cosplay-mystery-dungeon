package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.NumTween;
	
	/**
	 * Game object to display some text and float/fade away
	 * @author Switchbreak
	 */
	public class DeathFloater extends Entity 
	{
		// Assets
		[Embed(source = "../../img/Effects/skull.png")] protected static const SKULL:Class;
		
		// Constants
		protected static const DURATION:Number	= 1;
		protected static const DISTANCE:int		= 20;
		
		// Variables
		protected var floatTween:NumTween;
		protected var floaterImage:Image;
		protected var startY:int;
		
		/**
		 * Create and initialize the floater
		 * @param	setX		Starting position
		 * @param	setY		Starting position
		 */
		public function DeathFloater( setX:int, setY:int ) 
		{
			floaterImage = new Image( SKULL );
			floaterImage.originX = floaterImage.width >> 1;
			floaterImage.originY = floaterImage.height;
			
			super( setX, setY, floaterImage );
			
			layer	= GameWorld.HUD_LAYER;
			startY	= setY;
			
			floatTween = new NumTween( Finished, Tween.ONESHOT );
			addTween( floatTween );
			floatTween.tween( 0, 1, DURATION );
		}
		
		/**
		 * Handle motion/fadeout
		 */
		override public function update():void 
		{
			super.update();
			
			y = startY - (floatTween.value * DISTANCE);
			floaterImage.alpha = 1 - floatTween.value;
		}
		
		/**
		 * Remove floater from the world after it has faded out
		 */
		protected function Finished():void
		{
			world.remove( this );
		}
	}

}