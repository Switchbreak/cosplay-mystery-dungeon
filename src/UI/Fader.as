package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.VarTween;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Fader extends Entity 
	{
		protected var curtain:Image;
		protected var onComplete:Function;
		protected var fadeTween:VarTween;
		
		public function Fader() 
		{
			layer = int.MIN_VALUE;
			
			curtain = Image.createRect( FP.width, FP.height, 0, 0 );
			curtain.scrollX = curtain.scrollY = 0;
			
			super( 0, 0, curtain );
			
			fadeTween = new VarTween( Finish, Tween.PERSIST );
			addTween( fadeTween );
		}
		
		public function Start( fadeOut:Boolean, duration:Number, setOnComplete:Function ):void
		{
			onComplete	= setOnComplete;
			curtain.alpha = (fadeOut ? 0 : 1);
			fadeTween.tween( curtain, "alpha", (fadeOut ? 1 : 0), duration );
		}
		
		protected function Finish():void 
		{
			if( onComplete != null )
				onComplete();
		}
	}
}