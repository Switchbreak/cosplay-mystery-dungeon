package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.NumTween;
	
	/**
	 * Game object to display some text and float/fade away
	 * @author Switchbreak
	 */
	public class Floater extends Entity 
	{
		// Constants
		protected static const DURATION:Number	= 1;
		protected static const DISTANCE:int		= 20;
		
		// Variables
		protected var floatTween:NumTween;
		protected var floaterText:Text;
		protected var startY:int;
		
		/**
		 * Create and initialize the floater
		 * @param	text		Text to display
		 * @param	setX		Starting position
		 * @param	setY		Starting position
		 * @param	color		Text color
		 */
		public function Floater( text:String, setX:int, setY:int, color:uint ) 
		{
			floaterText = new Text( text, 0, 0, { color: color, size:20 } );
			floaterText.originX = floaterText.width >> 1;
			floaterText.originY = floaterText.height;
			
			super( setX, setY, floaterText );
			
			layer	= GameWorld.HUD_LAYER;
			startY	= setY;
			
			floatTween = new NumTween( Finished, Tween.ONESHOT );
			addTween( floatTween );
			floatTween.tween( 0, 1, DURATION );
		}
		
		/**
		 * Handle motion/fadeout
		 */
		override public function update():void 
		{
			super.update();
			
			y = startY - (floatTween.value * DISTANCE);
			floaterText.alpha = 1 - floatTween.value;
		}
		
		/**
		 * Remove floater from the world after it has faded out
		 */
		protected function Finished():void
		{
			world.remove( this );
		}
	}

}