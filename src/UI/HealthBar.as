package UI 
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import mx.utils.StringUtil;
	
	/**
	 * Shows health bar
	 * @author Switchbreak
	 */
	public class HealthBar extends Entity 
	{
		// Constants
		protected static const BAR_X:int			= 10;
		protected static const BAR_Y:int			= 30;
		protected static const BAR_WIDTH:int		= 130;
		protected static const BAR_HEIGHT:int		= 20;
		protected static const BAR_COLOR:int		= 0x0000FF;
		protected static const BORDER:int			= 2;
		protected static const COUNTER:String		= "HP: {0} / {1}";
		protected static const COUNTER_TEXT:Object	= { align: "center", color: 0xFFFFFF, width: BAR_WIDTH };
		
		// Variables
		protected var bar:Image;
		protected var counter:Text;
		
		/**
		 * Create and initialize the health bar
		 */
		public function HealthBar() 
		{
			var backgroundBitmap:BitmapData = new BitmapData( BAR_WIDTH, BAR_HEIGHT, true );
			backgroundBitmap.fillRect( new Rectangle( BORDER, BORDER, BAR_WIDTH - (BORDER << 1), BAR_HEIGHT - (BORDER << 1) ), 0xFF000000 );
			var background:Stamp = new Stamp( backgroundBitmap );
			
			counter = new Text( "", 0, -2, COUNTER_TEXT );
			
			bar = Image.createRect( 1, BAR_HEIGHT - (BORDER << 1), BAR_COLOR );
			bar.x = BORDER;
			bar.y = BORDER;
			
			UpdateBar();
			
			super( BAR_X, BAR_Y, new Graphiclist( background, bar, counter ) );
			
			graphic.scrollX	= 0;
			graphic.scrollY	= 0;
			layer			= GameWorld.HUD_LAYER;
			active			= false;
		}
		
		/**
		 * Updates the health bar to the player's current HP
		 */
		public function UpdateBar():void
		{
			bar.scaleX = (GameWorld.player.HP / Player.MAX_HP) * (BAR_WIDTH - (BORDER << 1));
			counter.text = StringUtil.substitute( COUNTER, GameWorld.player.HP, Player.MAX_HP );
		}
		
	}

}