package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import Types.Item;
	
	/**
	 * Inventory panel to display current character inventory
	 * @author Switchbreak
	 */
	public class Inventory extends Entity 
	{
		// Assets
		[Embed(source = "../../img/Inventory/inventory-panel.png")]	private static const INVENTORY_BAR:Class;
		[Embed(source = "../../img/Inventory/nav-button.png")]		private static const NAV_BUTTON:Class;
		[Embed(source = "../../snd/Inventory/InventoryOpen.mp3")]   private static const INVENTORY_OPEN_SND:Class;
		[Embed(source = "../../snd/Inventory/InventoryClose.mp3")]  private static const INVENTORY_CLOSE_SND:Class;
		
		// Constants
		static private const NAV_BUTTON_LEFT_X:int	= 10;
		static private const NAV_BUTTON_RIGHT_X:int	= FP.width - 74;
		static private const NAV_BUTTON_Y:int		= FP.height - 70;
		static private const NAV_BUTTON_WIDTH:uint	= 64;
		static private const NAV_BUTTON_HEIGHT:uint	= 64;
		static private const ITEM_WINDOW_X:int		= 87;
		static private const ITEM_WINDOW_Y:int		= FP.height - 70;
		static private const ITEM_WINDOW_WIDTH:int	= 80;
		static private const ITEM_WINDOW_COUNT:int	= 6;
		
		// Variables
		protected var _enabled:Boolean;
		protected var leftNav:Button;
		protected var rightNav:Button;
		protected var itemWindows:Vector.<ItemWindow>;
		protected var scrollIndex:int = 0;
		protected var itemDetail:ItemDetailPane = null;
		
		// Sounds
		protected var inventoryOpenSfx:Sfx  = new Sfx(INVENTORY_OPEN_SND);
		static protected const INVENTORY_OPEN_VOLUME:Number = 0.2;
		protected var inventoryCloseSfx:Sfx = new Sfx(INVENTORY_CLOSE_SND);
		static protected const INVENTORY_CLOSE_VOLUME:Number = 0.2;
		
		/**
		 * Create and initialize the inventory panel
		 * @param	showAtStart		Whether the panel should start enabled
		 */
		public function Inventory( showAtStart:Boolean = false ) 
		{
			layer = GameWorld.HUD_LAYER;
			
			graphic = new Graphiclist();
			graphic.scrollX = graphic.scrollY = 0;
			
			var inventoryBar:Stamp = new Stamp( INVENTORY_BAR );
			inventoryBar.y = FP.height - inventoryBar.height;
			addGraphic( inventoryBar );
			
			leftNav		= new Button( NAV_BUTTON, NAV_BUTTON_WIDTH, NAV_BUTTON_HEIGHT, NAV_BUTTON_LEFT_X, NAV_BUTTON_Y, false, ScrollLeft );
			rightNav	= new Button( NAV_BUTTON, NAV_BUTTON_WIDTH, NAV_BUTTON_HEIGHT, NAV_BUTTON_RIGHT_X, NAV_BUTTON_Y, true, ScrollRight );
			addGraphic( leftNav );
			addGraphic( rightNav );
			
			itemWindows = new Vector.<ItemWindow>();
			UpdateInventory();
			
			enabled = showAtStart;
		}
		
		/**
		 * Scroll the inventory list left
		 */
		public function ScrollLeft():void
		{
			if ( scrollIndex > 0 )
			{
				scrollIndex--;
				UpdateInventory();
			}
		}
		
		/**
		 * Scroll the inventory list right
		 */
		public function ScrollRight():void
		{
			if ( scrollIndex < GameWorld.player.inventory.length - ITEM_WINDOW_COUNT )
			{
				scrollIndex++;
				UpdateInventory();
			}
		}
		
		public function ScrollToNewest():void
		{
			scrollIndex = Math.max( GameWorld.player.inventory.length - ITEM_WINDOW_COUNT, 0 );
			UpdateInventory();
		}
		
		/**
		 * Select an item in the inventory
		 * @param	item	Item selected
		 */
		public function SelectItem( item:Item ):void
		{
			if ( itemDetail != null )
				CloseItemDetail();
			
			var sell:Boolean = false;
			if ( world.classCount( BuyPanel ) > 0 )
				sell = true;
			
			itemDetail = new ItemDetailPane( item, CloseItemDetail, sell );
			addGraphic( itemDetail );
		}
		
		/**
		 * Close an open item detail pane
		 */
		public function CloseItemDetail():void
		{
			(graphic as Graphiclist).remove( itemDetail );
			itemDetail = null;
		}
		
		/**
		 * Updates the inventory display
		 */
		public function UpdateInventory():void
		{
			RemoveAllItems();
			DisplayPlayerInventory();
			RefreshItemDetail();
		}
		
		/**
		 * Remove all item windows currently displayed
		 */
		protected function RemoveAllItems():void
		{
			for each( var itemWindow:ItemWindow in itemWindows )
			{
				(graphic as Graphiclist).remove( itemWindow );
			}
			itemWindows.splice( 0, itemWindows.length );
		}
		
		/**
		 * Get the player's current inventory and display it
		 */
		protected function DisplayPlayerInventory():void
		{
			var itemX:int = ITEM_WINDOW_X;
			for (var i:int = scrollIndex; i < scrollIndex + ITEM_WINDOW_COUNT && i < GameWorld.player.inventory.length; ++i) 
			{
				var itemWindow:ItemWindow = new ItemWindow( itemX, ITEM_WINDOW_Y, GameWorld.player.inventory[i], SelectItem );
				addGraphic( itemWindow );
				itemWindows.push( itemWindow );
				
				itemX += ITEM_WINDOW_WIDTH;
			}
		}
		
		/**
		 * Refresh the data in the item detail pane if an item is selected and
		 * the item is still in the player's inventory, otherwise close the pane
		 */
		protected function RefreshItemDetail():void
		{
			if ( itemDetail != null )
			{
				if ( GameWorld.player.inventory.indexOf( itemDetail.item ) == -1 )
					CloseItemDetail();
				else 
					SelectItem( itemDetail.item );
			}
		}
		
		/**
		 * Set to true to show panel and have it take input, false to hide
		 */
		public function get enabled():Boolean
		{
			return _enabled;
		}
		public function set enabled( value:Boolean ):void
		{
		    if (_enabled == false && value == true)
			{
		        inventoryOpenSfx.play(INVENTORY_OPEN_VOLUME);
		    }
		    if (_enabled == true && value == false)
			{
		        inventoryCloseSfx.play(INVENTORY_CLOSE_VOLUME);
		    }
			
			_enabled		= value;
			active			= value;
			visible			= value;
			graphic.active	= value;
		}
	}
}