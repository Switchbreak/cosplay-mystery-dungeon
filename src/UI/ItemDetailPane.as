package UI 
{
	import mx.core.ButtonAsset;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import Types.Item;
	
	/**
	 * UI Pane to show item detail
	 * @author Switchbreak
	 */
	public class ItemDetailPane extends Graphiclist 
	{
		// Assets
		[Embed(source = "../../img/Inventory/detail-panel.png")]		private static const DETAIL_PANEL:Class;
		[Embed(source = "../../img/Inventory/CloseButton.png")]			private static const CLOSE_BUTTON:Class;
		[Embed(source = "../../img/Inventory/equip-button-sm.png")]		private static const EQUIP_BUTTON:Class;
		[Embed(source = "../../img/Inventory/drop-button-sm.png")]		private static const THROW_BUTTON:Class;
		[Embed(source = "../../img/Inventory/use-button-sm.png")]		private static const USE_BUTTON:Class;
		[Embed(source = "../../img/Inventory/wear-button-sm.png")]		private static const WEAR_BUTTON:Class;
		[Embed(source = "../../img/Inventory/remove-button-sm.png")]	private static const REMOVE_BUTTON:Class;
		[Embed(source = "../../img/Inventory/shoot-button-sm.png")]		private static const SHOOT_BUTTON:Class;
		[Embed(source = "../../img/Inventory/sell-button-sm.png")]		private static const SELL_BUTTON:Class;
		
		// Constants
		protected static const PANE_WIDTH:int				= 228;
		protected static const PANE_HEIGHT:int				= 280;
		protected static const CLOSE_BUTTON_WIDTH:int		= 32;
		protected static const CLOSE_BUTTON_HEIGHT:int		= 32;
		protected static const USE_BUTTON_WIDTH:int			= 60;
		protected static const USE_BUTTON_HEIGHT:int		= 24;
		protected static const THROW_BUTTON_WIDTH:int		= 60;
		protected static const THROW_BUTTON_HEIGHT:int		= 24;
		protected static const EQUIP_BUTTON_WIDTH:int		= 69;
		protected static const EQUIP_BUTTON_HEIGHT:int		= 24;
		protected static const WEAR_BUTTON_WIDTH:int		= 60;
		protected static const WEAR_BUTTON_HEIGHT:int		= 24;
		protected static const REMOVE_BUTTON_WIDTH:int		= 69;
		protected static const REMOVE_BUTTON_HEIGHT:int		= 24;
		protected static const SHOOT_BUTTON_WIDTH:int		= 69;
		protected static const SHOOT_BUTTON_HEIGHT:int		= 24;
		protected static const SELL_BUTTON_WIDTH:int		= 60;
		protected static const SELL_BUTTON_HEIGHT:int		= 24;
		protected static const CLOSE_BUTTON_X:int			= FP.width - 33;
		protected static const CLOSE_BUTTON_Y:int			= 10;
		protected static const ITEM_WINDOW_X:int			= FP.width - 145;
		protected static const ITEM_WINDOW_Y:int			= 40;
		protected static const ITEM_NAME_Y:int				= 105;
		protected static const ITEM_NAME_TEXT:Object		= { align: "center", color: 0xFFFFFF, width: PANE_WIDTH };
		protected static const ITEM_DESCRIPTION_X:int		= 5;
		protected static const ITEM_DESCRIPTION_Y:int		= 125;
		protected static const ITEM_DESCRIPTION_TEXT:Object	= { color: 0xFFFFFF, width: PANE_WIDTH - 10, wordWrap: true, height: 90, resizable: false };
		protected static const ITEM_STATUS_X:int			= 5;
		protected static const ITEM_STATUS_Y:int			= 215;
		protected static const ITEM_STATUS_TEXT:Object		= { color: 0x00FF00, width: PANE_WIDTH - 10, resizable: false };
		protected static const ITEM_BUTTON_X:int			= 5;
		protected static const ITEM_BUTTON_Y:int			= 240;
		
		// Variables
		public var		item:Item;
		protected var	eventClose:Function
		
		/**
		 * Create and initialize the item detail pane
		 * @param	setItem			Item to show
		 * @param	setEventClose	Function to call to close the detail pane
		 */
		public function ItemDetailPane( setItem:Item, setEventClose:Function, sell:Boolean ) 
		{
			item		= setItem;
			eventClose	= setEventClose;
			
			var backgroundImage:Stamp = new Stamp( DETAIL_PANEL );
			backgroundImage.x = FP.width - PANE_WIDTH;
			
			var closeButton:Button = new Button( CLOSE_BUTTON, CLOSE_BUTTON_WIDTH, CLOSE_BUTTON_HEIGHT, CLOSE_BUTTON_X, CLOSE_BUTTON_Y, false, eventClose );
			
			var itemWindow:ItemWindow = new ItemWindow( ITEM_WINDOW_X, ITEM_WINDOW_Y, setItem, null );
			itemWindow.active = false;
			
			var itemName:Text = new Text( item.name, FP.width - PANE_WIDTH, ITEM_NAME_Y, ITEM_NAME_TEXT );
			var itemDescription:Text = new Text( item.description, FP.width - PANE_WIDTH + ITEM_DESCRIPTION_X, ITEM_DESCRIPTION_Y, ITEM_DESCRIPTION_TEXT );
			var itemStatus:Text = new Text( GetItemStatus(), FP.width - PANE_WIDTH + ITEM_STATUS_X, ITEM_STATUS_Y, ITEM_STATUS_TEXT );
			
			super( backgroundImage, closeButton, itemWindow, itemName, itemDescription, itemStatus );
			
			active		= true;
			relative	= false;
			scrollX		= 0;
			scrollY		= 0;
			
			AddButtons( sell );
		}
		
		/**
		 * Checks if the item is being worn or equipped by the player
		 * @return	A description of the status to display
		 */
		protected function GetItemStatus():String
		{
			if ( item.equipped )
				return "Weapon is equipped";
			if ( item.wearing )
				return "Currently wearing item";
			
			return "";
		}
		
		/**
		 * Add buttons specific to available item actions
		 * @param	sell			True if items can be sold (if adjacent to a
		 * 							vendor)
		 */
		protected function AddButtons( sell:Boolean ):void
		{
			var buttonX:int = FP.width - PANE_WIDTH + ITEM_BUTTON_X;
			if ( item.usable )
			{
				add( new Button( USE_BUTTON, USE_BUTTON_WIDTH, USE_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, UseItem ) );
				buttonX += USE_BUTTON_WIDTH + ITEM_BUTTON_X;
			}
			if ( item.throwable )
			{
				add( new Button( THROW_BUTTON, THROW_BUTTON_WIDTH, THROW_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, ThrowItem ) );
				buttonX += THROW_BUTTON_WIDTH + ITEM_BUTTON_X;
			}
			if ( item.equipable )
			{
				if ( item.equipped )
				{
					add( new Button( REMOVE_BUTTON, REMOVE_BUTTON_WIDTH, REMOVE_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, RemoveItem ) );
					buttonX += REMOVE_BUTTON_WIDTH + ITEM_BUTTON_X;
				}
				else
				{
					add( new Button( EQUIP_BUTTON, EQUIP_BUTTON_WIDTH, EQUIP_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, EquipItem ) );
					buttonX += EQUIP_BUTTON_WIDTH + ITEM_BUTTON_X;
				}
			}
			if ( item.wearable )
			{
				if ( item.wearing )
				{
					add( new Button( REMOVE_BUTTON, REMOVE_BUTTON_WIDTH, REMOVE_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, RemoveItem ) );
					buttonX += REMOVE_BUTTON_WIDTH + ITEM_BUTTON_X;
				}
				else 
				{
					add( new Button( WEAR_BUTTON, WEAR_BUTTON_WIDTH, WEAR_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, WearItem ) );
					buttonX += WEAR_BUTTON_WIDTH + ITEM_BUTTON_X;
				}
			}
			if ( sell )
			{
				add( new Button( SELL_BUTTON, SELL_BUTTON_WIDTH, SELL_BUTTON_HEIGHT, buttonX, ITEM_BUTTON_Y, false, SellItem ) );
				buttonX += SELL_BUTTON_WIDTH + ITEM_BUTTON_X;
			}
		}
		
		/**
		 * Use the selected item
		 */
		protected function UseItem():void
		{
			GameWorld.player.UseItem( item );
		}
		
		/**
		 * Throw the selected item
		 */
		protected function ThrowItem():void
		{
			GameWorld.player.ThrowItem( item );
		}
		
		/**
		 * Equip the selected item
		 */
		protected function EquipItem():void
		{
			GameWorld.player.EquipItem( item );
		}
		
		/**
		 * Wear the selected item
		 */
		protected function WearItem():void
		{
			GameWorld.player.WearItem( item );
		}
		
		/**
		 * Remove the selected item
		 */
		protected function RemoveItem():void
		{
			GameWorld.player.RemoveItem( item );
		}
		
		/**
		 * Sell the selected item
		 */
		protected function SellItem():void
		{
			GameWorld.player.SellItem( item );
		}
	}
}