package UI 
{
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.utils.Input;
	import Types.Item;
	
	/**
	 * Pane to show an item for the inventory panel
	 * @author Switchbreak
	 */
	public class ItemWindow extends Graphiclist 
	{
		// Assets
		[Embed(source = "../../img/Inventory/item-window.png")] private static const ITEM_WINDOW:Class;
		
		// Constants
		static private const WINDOW_WIDTH:int		= 64;
		static private const WINDOW_HEIGHT:int		= 64;
		static private const UNPRESSED_FRAME:int	= 0;
		static private const PRESSED_FRAME:int		= 1;
		
		// Variables
		protected var item:Item;
		protected var selectedEvent:Function;
		
		/**
		 * Create and initialize the item window
		 * @param	setX				Position to show the item window
		 * @param	setY				Position to show the item window
		 * @param	setItem				Item to display
		 * @param	setSelectedEvent	Function to call when the window is
		 * 								clicked, will pass a reference to the
		 * 								item as a parameter
		 */
		public function ItemWindow( setX:int, setY:int, setItem:Item, setSelectedEvent:Function )
		{
			var itemWindow:Button = new Button( ITEM_WINDOW, WINDOW_WIDTH, WINDOW_HEIGHT, setX, setY, false, ItemSelected );
			
			super( itemWindow );
			
			if ( setItem != null )
			{
				var itemDisplay:Graphic;
				itemDisplay		= setItem.graphic;
				itemDisplay.x	= (itemWindow.width >> 1) - (setItem.width >> 1);
				itemDisplay.y	= (itemWindow.height >> 1) - (setItem.height >> 1);
				add( itemDisplay );
				
				active = true;
			}
			else
			{
				active = false;
			}
			
			item			= setItem;
			selectedEvent	= setSelectedEvent;
			x				= setX;
			y				= setY;
			relative		= false;
			scrollX			= 0;
			scrollY			= 0;
		}
		
		/**
		 * Called when the item window is clicked
		 */
		public function ItemSelected():void
		{
			selectedEvent( item );
		}
	}
}