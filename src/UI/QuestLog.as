package UI
{
	import flash.display.BitmapData;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Quest log UI pane
	 * @author Switchbreak
	 */
	public class QuestLog extends Entity 
	{
		// Assets
		[Embed(source = "../../img/QuestLogFade.png")] protected static const FADE:Class;
		
		// Constants
		protected static const LINE_COUNT:int	= 5;
		protected static const LOG_X:int		= 150;
		protected static const LOG_Y:int		= 0;
		protected static const LOG_WIDTH:int	= 380;
		protected static const LOG_HEIGHT:int	= 100;
		protected static const LOG_TEXT:Object	=
		{
			color:		0xFF8800,
			width:		LOG_WIDTH - 5,
			wordWrap:	false,
			height:		LOG_HEIGHT,
			resizable:	false,
			scrollX:	0,
			scrollY:	0
		};
		
		// Variables
		protected var questLogText:Text;
		protected var lines:Vector.<String>;
		
		/**
		 * Create and initialize the quest log
		 */
		public function QuestLog() 
		{
			var background:Image = Image.createRect( LOG_WIDTH, LOG_HEIGHT, 0, 0.5 );
			background.scrollX = background.scrollY = 0;
			
			questLogText = new Text( "", 5, 0, LOG_TEXT );
			
			super( LOG_X, LOG_Y, new Graphiclist( background, questLogText ) );
			
			active	= false;
			layer	= GameWorld.HUD_LAYER;
			lines	= new Vector.<String>();
			Clear();
		}
		
		/**
		 * Add a line of text to the quest log
		 */
		public function AddLine( line:String ):void
		{
			if ( lines.length >= LINE_COUNT )
			{
				lines.shift();
				if ( questLogText.drawMask == null )
					questLogText.drawMask = FP.getBitmap( FADE );
			}
			lines.push( line );
			
			questLogText.text = lines.join( "\n" );
		}
		
		/**
		 * Clear the quest log
		 */
		public function Clear():void
		{
			lines.splice( 0, lines.length );
			questLogText.text = "";
			questLogText.drawMask = null;
		}
	}
}