package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import Types.Item;
	
	/**
	 * Game object for an item out on the world map
	 * @author Switchbreak
	 */
	public class WorldItem extends Entity 
	{
		// Variables
		public var		item:Item;
		public var		row:int;
		public var		column:int;
		
		/**
		 * Create and initialize the world item
		 * @param	setRow		Position on the map
		 * @param	setColumn	Position on the map
		 * @param	setItem		Item contained in the object
		 */
		public function WorldItem( setRow:int, setColumn:int, setItem:Item )
		{
			super( setColumn * FloorMap.TILE_WIDTH + (FloorMap.TILE_WIDTH >> 1) - (setItem.width >> 1),
					setRow * FloorMap.TILE_HEIGHT + (FloorMap.TILE_HEIGHT >> 1) - (setItem.height >> 1),
					setItem.graphic );
			
			item	= setItem;
			row		= setRow;
			column	= setColumn;
			active	= false;
			layer	= -1 - row;
		}
	}
}